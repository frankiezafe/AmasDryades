/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   arduinomanager.cpp
 * Author: frankiezafe
 * 
 * Created on June 18, 2018, 8:02 AM
 */

#include "arduinomanager.h"

arduinomanager* _arduinomgr = 0;

arduinomanager* arduinomanager::get() {
    if (!_arduinomgr) {
        _arduinomgr = new arduinomanager();
    }
    return _arduinomgr;
}

void arduinomanager::push(arduino_data& data) {

    get();

    _arduinomgr->lock();

    std::stringstream ss;
    ss << "arduinomanager::push, pushing " << data.UID << ":" << data.path;
    ofLogNotice(ss.str());

    _arduinomgr->_connection_requests.push_back(data);
    _arduinomgr->_connection_request_wait =
            _arduinomgr->_connection_request_interval;
    _arduinomgr->_connection_request_check = true;

    _arduinomgr->unlock();

}

void arduinomanager::push(arduino_data_vector& list) {

    get();

    _arduinomgr->lock();

    arduino_data_iterator it = list.begin();
    arduino_data_iterator ite = list.end();

    for (; it != ite; ++it) {

        arduino_data& ad = (*it);
        std::stringstream ss;
        ss << "arduinomanager::push, pushing " <<
                ad.UID << ":" <<
                ad.path;
        ofLogNotice(ss.str());
        _arduinomgr->_connection_requests.push_back(ad);

    }
    _arduinomgr->_connection_request_wait =
            _arduinomgr->_connection_request_interval;
    _arduinomgr->_connection_request_check = true;

    _arduinomgr->unlock();

}

void arduinomanager::map(flatmap* m) {

    get();

    // modification of columns in all arduino created

    _arduinomgr->lock();

    _arduinomgr->_map = m;
    std::map< uint32_t, arduinowrapper* >::iterator it =
            _arduinomgr->_arduinos.begin();
    std::map< uint32_t, arduinowrapper* >::iterator ite =
            _arduinomgr->_arduinos.end();
    for (; it != ite; ++it) {
        (*it).second->map(m);
    }

    _arduinomgr->unlock();


}

void arduinomanager::max_sender(ofxOscSender* oscsend) {

    get();

    // modification of columns in all arduino created

    _arduinomgr->lock();

    _arduinomgr->_max_sender = oscsend;
    std::map< uint32_t, arduinowrapper* >::iterator it =
            _arduinomgr->_arduinos.begin();
    std::map< uint32_t, arduinowrapper* >::iterator ite =
            _arduinomgr->_arduinos.end();
    for (; it != ite; ++it) {
        (*it).second->max_sender(oscsend);
    }

    _arduinomgr->unlock();


}

void arduinomanager::simulate(
        const uint32_t& col_id,
        const uint8_t& sensor_id,
        const float& v) {

    if (_arduinomgr && _arduinomgr->_map) {

        _arduinomgr->lock();

        column* target = 0;
        vector< column >::iterator it = _arduinomgr->_map->columns().begin();
        vector< column >::iterator ite = _arduinomgr->_map->columns().end();
        for (; it != ite; ++it) {
            if (col_id == (*it).UID()) {
                target = &(*it);
                break;
            }
        }

        if (target) {
            target->update_thread_sensor(sensor_id, v);
        }
        
        _arduinomgr->unlock();

    }

}

void arduinomanager::update() {

    if (_arduinomgr && _arduinomgr->_map) {

        _arduinomgr->lock();

        vector< column >::iterator it = _arduinomgr->_map->columns().begin();
        vector< column >::iterator ite = _arduinomgr->_map->columns().end();
        for (; it != ite; ++it) {
            (*it).sync();
        }

        _arduinomgr->unlock();

    }

}

arduinomanager::arduinomanager() :
ofThread(),
_now(0),
_lasttime(0),
_delta(0),
_connection_request_wait(1000),
_connection_request_interval(1000),
_connection_request_check(false),
_map(0) {

    startThread();

    std::cout <<
            STD_HR << std::endl <<
            "arduinomanager, available serials" << std::endl;

    vector <ofSerialDeviceInfo> serial_infos = _serial.getDeviceList();
    vector <ofSerialDeviceInfo>::iterator its = serial_infos.begin();
    vector <ofSerialDeviceInfo>::iterator itse = serial_infos.end();
    for (; its != itse; ++its) {

        std::cout << "\t" <<
                (*its).getDeviceID() << ", " <<
                (*its).getDevicePath() <<
                std::endl;

    }
    
    std::cout << STD_HR << std::endl;

}

arduinomanager::~arduinomanager() {

    stopThread();

    std::map< uint32_t, arduinowrapper* >::iterator it = _arduinos.begin();
    std::map< uint32_t, arduinowrapper* >::iterator ite = _arduinos.end();
    for (; it != ite; ++it) {
        delete (*it).second;
    }
    _arduinos.clear();

}

int arduinomanager::get_device_id(const std::string& path) {

    vector <ofSerialDeviceInfo> serial_infos = _serial.getDeviceList();
    vector <ofSerialDeviceInfo>::iterator its = serial_infos.begin();
    vector <ofSerialDeviceInfo>::iterator itse = serial_infos.end();

    for (; its != itse; ++its) {

        if (path.compare((*its).getDevicePath()) == 0) {
            return (*its).getDeviceID();
        }

    }

    return -1;

}

void arduinomanager::parse_connection_requests() {

    arduino_data_iterator itr = _connection_requests.begin();
    arduino_data_iterator itre = _connection_requests.end();
    arduino_data_vector to_keep;

    for (; itr != itre; ++itr) {

        arduino_data& ad = (*itr);
        std::string& path = ad.path;

        int sel = get_device_id(path);

        // we have to wait for the arduino to boot, so skipping the other requests
        if (sel == -1) {

            //            ofLogError("arduinomanager::parse_connection_requests, serial not found " + path);
            to_keep.push_back(ad);

        } else {

            ofLogNotice("arduinomanager::parse_connection_requests, serial found! " + path);
            _active_serials.push_back(ad);
            boot_arduino(ad);

        }

    }

    // adapting the connections to check
    _connection_requests.swap(to_keep);

}

void arduinomanager::boot_arduino(const arduino_data& adata) {

    if (_arduinos.find(adata.UID) != _arduinos.end()) {

        std::stringstream ss;
        ss << "arduinomanager::boot_arduino, duplicate id! " <<
                adata.UID << ":" <<
                adata.path;
        ofLogError(ss.str());
        return;

    }

    arduinowrapper* ard = new arduinowrapper(adata);
    ard->map(_map);
    ard->max_sender(_max_sender);
    ard->start();
    _arduinos[adata.UID] = ard;

}

void arduinomanager::threadedFunction() {

    while (isThreadRunning()) {

        lock();

        _now = ofGetElapsedTimeMillis();
        if (_lasttime != 0) {
            _delta = _now - _lasttime;
        }
        _lasttime = _now;

        if (_connection_request_wait > 0) {
            if (_connection_request_wait <= _delta) {
                uint64_t diff = _delta - _connection_request_wait;
                _connection_request_wait = _connection_request_interval - diff;
                _connection_request_check = true;
            } else {
                _connection_request_wait -= _delta;
            }
        }

        if (
                _connection_request_check &&
                !_connection_requests.empty()) {

            _connection_request_check = false;
            parse_connection_requests();

        }

        std::map< uint32_t, arduinowrapper* >::iterator it = _arduinos.begin();
        std::map< uint32_t, arduinowrapper* >::iterator ite = _arduinos.end();
        for (; it != ite; ++it) {
            (*it).second->update();
        }

        unlock();

        sleep(10);

    }

}

