/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   texture_manager.cpp
 * Author: frankiezafe
 * 
 * Created on June 17, 2018, 4:58 PM
 */

#include "texturemanager.h"

texturemanager* _textmgr = 0;

texturemanager* texturemanager::get() {
    if ( !_textmgr ) {
        _textmgr = new texturemanager();
    }
    return _textmgr;
}

void texturemanager::load( std::string name, std::string path ) {

    get();
    
    ofImage* img = new ofImage();
    if ( img->load( path ) ) {
        if ( _textmgr->texture_map.find( name ) == _textmgr->texture_map.end() ) {
            _textmgr->texture_map[name] = img;
            _textmgr->path_map[name] = path;
        } else {
            ofLogError( "texturemanager::load, duplicated key '" + name + "'!" );
        }
    } else {
        delete img;
    }

}

ofImage* texturemanager::get( std::string name ) {
    
    if ( 
        !_textmgr ||
        _textmgr->texture_map.find( name ) == _textmgr->texture_map.end()    
        ) {
        ofLogError( "texturemanager::get, no key '" + name + "'!" );
        return 0;
    }
    return _textmgr->texture_map[name];
    
}

void texturemanager::purge() {

    if ( _textmgr ) {
        delete _textmgr;
        _textmgr = 0;
    }
    
}

std::string texturemanager::json() {
    
    std::stringstream ss;
    ss << "\"textures\":[";
    
    if ( _textmgr ) {
        
        std::map< std::string, std::string >::iterator it = _textmgr->path_map.begin();
        std::map< std::string, std::string >::iterator itb = _textmgr->path_map.begin();
        std::map< std::string, std::string >::iterator ite = _textmgr->path_map.end();
        
        for( ; it != ite; ++it ) {
            
            if ( it != itb ) {
                ss << ",";
            }
            ss << "{\"name\":\"" << it->first << "\"," <<
                    "\"path\":\"" << it->second << "\"}";
            
        }
        
    }
    
    ss << "]";
    
    return ss.str();
    
}

texturemanager::texturemanager() {
}

texturemanager::~texturemanager() {
    
    std::map< std::string, ofImage* >::iterator it = texture_map.begin();
    std::map< std::string, ofImage* >::iterator ite = texture_map.end();
    for( ; it != ite; ++it ) {
        delete it->second;
    }
    texture_map.clear();
    
}

