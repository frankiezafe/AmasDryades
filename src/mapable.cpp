/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   mappable.cpp
 * Author: frankiezafe
 * 
 * Created on June 17, 2018, 3:53 PM
 */

#include "mapable.h"

mapable::mapable() :
_colA(numeric_limits<uint32_t>::max()),
_colB(numeric_limits<uint32_t>::max()),
_viewport(0),
_subdivide(MAPABLE_SUBDIVIDE),
_rgba(1, 1, 1, 1),
_texture(0),
_generate_hanlders(true) {

    init();

    pack();

}

mapable::mapable(const ofVec3f& pt0, const ofVec3f& pt1) :
_colA(numeric_limits<uint32_t>::max()),
_colB(numeric_limits<uint32_t>::max()),
_viewport(0),
_subdivide(MAPABLE_SUBDIVIDE),
_rgba(1, 1, 1, 1),
_texture(0),
_generate_hanlders(false) {

    _hanlders.resize(4);
    _hanlders[0] = pt0;
    _hanlders[1] = pt1;

    init();

    pack();

}

mapable::mapable(const mapable_data& src) :
_viewport(0),
_rgba(1, 1, 1, 1),
_texture(0),
_generate_hanlders(false) {

    init();

    _colA = src.colA;
    _colB = src.colB;
    _subdivide = src.subdivide;
    _viewport = src.viewport;

    if (src.rgba.size() == 4) {
        _rgba.x = src.rgba[0];
        _rgba.y = src.rgba[1];
        _rgba.z = src.rgba[2];
        _rgba.w = src.rgba[3];
    }

    if (src.uv.size() == 8) {
        for (uint8_t i = 0; i < 4; ++i) {
            uv_coords[i].x = src.uv[ i * 2 ];
            uv_coords[i].y = src.uv[ i * 2 + 1 ];
        }
    }

    if (src.handlers.size() == _hanlders.size() * 3) {
        for (uint8_t i = 0; i < _hanlders.size(); ++i) {
            _hanlders[i].x = src.handlers[ i * 3 ];
            _hanlders[i].y = src.handlers[ i * 3 + 1 ];
            _hanlders[i].z = src.handlers[ i * 3 + 2 ];
        }
    }

    set_texture(src.texture_name);

    pack();

}

void mapable::init() {
    
    uv_coords.resize(4);
    uv_coords[0].set(0, 0);
    uv_coords[1].set(1, 0);
    uv_coords[2].set(1, 1);
    uv_coords[3].set(0, 1);

    _hanlders.resize(4);

    if (_generate_hanlders) {
        _hanlders[0] = ofVec3f(0.2, 0.1, 0);
        _hanlders[1] = ofVec3f(0.8, 0.9, 0);
    }

    ofVec3f dir = _hanlders[1] - _hanlders[0];
    dir.normalize();

    ofVec3f perp(dir);
    perp.cross(ofVec3f(0, 0, 1));

    float width = 0.01;

    _hanlders[2] = ofVec3f(_hanlders[0] + perp * width);
    _hanlders[3] = ofVec3f(_hanlders[1] + perp * width);

    _perpendiculars.resize(4);
    
    _corners.resize(4);

}

mapable::~mapable() {
}

std::string mapable::json() {

    std::stringstream ss;
    ss << "{";
    ss << "\"viewport\":" << int(_viewport) << ",";
    ss << "\"texture_name\":\"" << _texture_name << "\",";
    ss << "\"subdivide\":" << int(_subdivide) << ",";
    ss << "\"columns\":[" << _colA << "," << _colB << "],";
    ss << "\"rgba\":[" <<
            _rgba.x << "," <<
            _rgba.y << "," <<
            _rgba.z << "," <<
            _rgba.w << "],";
    ss << "\"handlers\":[";
    for (uint32_t i = 0; i < _hanlders.size(); ++i) {
        if (i > 0) {
            ss << ",";
        }
        ss << "[" <<
                _hanlders[i].x << "," <<
                _hanlders[i].y << "," <<
                _hanlders[i].z << "]";
    }
    ss << "],";
    ss << "\"uv\":[";
    for (uint32_t i = 0; i < 4; ++i) {
        if (i > 0) {
            ss << ",";
        }
        ss << "[" <<
                uv_coords[i].x << "," <<
                uv_coords[i].y << "]";
    }
    ss << "]}";

    return ss.str();

}

void mapable::refresh() {

    set_texture(_texture_name);
    pack();

}

void mapable::pack() {
    
    _corners[0] = _hanlders[0] - (_hanlders[2] - _hanlders[0]);
    _corners[1] = _hanlders[0] + (_hanlders[2] - _hanlders[0]);
    _corners[2] = _hanlders[1] + (_hanlders[3] - _hanlders[1]);
    _corners[3] = _hanlders[1] - (_hanlders[3] - _hanlders[1]);

    _center = _hanlders[0] + (_hanlders[1] - _hanlders[0]) * 0.5;

    _perpendiculars[0] = (_hanlders[2] - _hanlders[0]);
    _perpendiculars[0].normalize();
    _perpendiculars[0].cross(ofVec3f(0, 0, 1));
    _perpendiculars[1] = (_hanlders[1] - _hanlders[0]);
    _perpendiculars[1].normalize();
    _perpendiculars[1].cross(ofVec3f(0, 0, 1));
    _perpendiculars[2] = (_hanlders[1] - _hanlders[3]);
    _perpendiculars[2].normalize();
    _perpendiculars[2].cross(ofVec3f(0, 0, 1));
    _perpendiculars[3] = (_hanlders[0] - _hanlders[1]);
    _perpendiculars[3].normalize();
    _perpendiculars[3].cross(ofVec3f(0, 0, 1));

    _mesh.clear();
    _mesh.setMode(OF_PRIMITIVE_TRIANGLES);

    ofVec3f right_gap =
            (
            (_hanlders[1] - (_hanlders[3] - _hanlders[1])) -
            (_hanlders[0] - (_hanlders[2] - _hanlders[0]))
            ) /
            _subdivide;

    ofVec3f left_gap =
            (
            _hanlders[3] -
            _hanlders[2]
            ) /
            _subdivide;

    ofVec2f right_uv_gap = (uv_coords[3] - uv_coords[0]) / _subdivide;
    ofVec2f left_uv_gap = (uv_coords[2] - uv_coords[1]) / _subdivide;

    ofVec3f right_top = _hanlders[0] - (_hanlders[2] - _hanlders[0]);
    ofVec3f left_top = _hanlders[0] + (_hanlders[2] - _hanlders[0]);
    ofVec2f right_uv_top = uv_coords[0];
    ofVec2f left_uv_top = uv_coords[1];

    ofVec3f top_gap = (left_top - right_top) / _subdivide;
    ofVec2f top_uv_gap = (left_uv_top - right_uv_top) / _subdivide;

    for (uint8_t y = 1; y <= _subdivide; ++y) {

        ofVec3f right_bottom = right_top + right_gap;
        ofVec3f left_bottom = left_top + left_gap;

        ofVec2f right_uv_bottom = right_uv_top + right_uv_gap;
        ofVec2f left_uv_bottom = left_uv_top + left_uv_gap;

        ofVec3f bottom_gap = (left_bottom - right_bottom) / _subdivide;
        ofVec3f bottom_uv_gap = (left_uv_bottom - right_uv_bottom) / _subdivide;

        ofVec3f top_v0 = right_top;
        ofVec3f top_v1 = right_top + top_gap;
        ofVec3f bottom_v0 = right_bottom;
        ofVec3f bottom_v1 = right_bottom + bottom_gap;

        ofVec2f top_uv_v0 = right_uv_top;
        ofVec2f top_uv_v1 = right_uv_top + top_uv_gap;
        ofVec2f bottom_uv_v0 = right_uv_bottom;
        ofVec2f bottom_uv_v1 = right_uv_bottom + bottom_uv_gap;

        for (uint8_t x = 1; x <= _subdivide; ++x) {

            float d01 = top_v0.distance(bottom_v1);
            float d10 = top_v1.distance(bottom_v0);

            if (d01 <= d10) {

                _mesh.addVertex(top_v0);
                _mesh.addVertex(top_v1);
                _mesh.addVertex(bottom_v1);
                _mesh.addVertex(bottom_v1);
                _mesh.addVertex(bottom_v0);
                _mesh.addVertex(top_v0);

                _mesh.addTexCoord(top_uv_v0);
                _mesh.addTexCoord(top_uv_v1);
                _mesh.addTexCoord(bottom_uv_v1);
                _mesh.addTexCoord(bottom_uv_v1);
                _mesh.addTexCoord(bottom_uv_v0);
                _mesh.addTexCoord(top_uv_v0);

            } else {

                _mesh.addVertex(top_v1);
                _mesh.addVertex(bottom_v1);
                _mesh.addVertex(bottom_v0);
                _mesh.addVertex(bottom_v0);
                _mesh.addVertex(top_v0);
                _mesh.addVertex(top_v1);

                _mesh.addTexCoord(top_uv_v1);
                _mesh.addTexCoord(bottom_uv_v1);
                _mesh.addTexCoord(bottom_uv_v0);
                _mesh.addTexCoord(bottom_uv_v0);
                _mesh.addTexCoord(top_uv_v0);
                _mesh.addTexCoord(top_uv_v1);

            }

            top_v0 = top_v1;
            top_v1 += top_gap;
            bottom_v0 = bottom_v1;
            bottom_v1 += bottom_gap;

            top_uv_v0 = top_uv_v1;
            top_uv_v1 += top_uv_gap;
            bottom_uv_v0 = bottom_uv_v1;
            bottom_uv_v1 += bottom_uv_gap;

        }

        right_top = right_bottom;
        left_top = left_bottom;
        top_gap = bottom_gap;

        right_uv_top = right_uv_bottom;
        left_uv_top = left_uv_bottom;
        top_uv_gap = bottom_uv_gap;

    }


}

bool mapable::hit(const ofVec3f& pt) {

    ofVec3f rel = pt - (_hanlders[0] - (_hanlders[2] - _hanlders[0]));
    rel.normalize();
    if (rel.dot(_perpendiculars[0]) > 0) {
        return false;
    }
    rel = pt - _hanlders[2];
    rel.normalize();
    if (rel.dot(_perpendiculars[1]) > 0) {
        return false;
    }
    rel = pt - _hanlders[3];
    rel.normalize();
    if (rel.dot(_perpendiculars[2]) > 0) {
        return false;
    }
    rel = pt - (_hanlders[1] - (_hanlders[3] - _hanlders[1]));
    rel.normalize();
    if (rel.dot(_perpendiculars[3]) > 0) {
        return false;
    }
    return true;

}

void mapable::set_texture(std::string name) {

    _texture_name = name;
    _texture = texturemanager::get(_texture_name);

}

void mapable::set_handler(ofVec3f* handler, const ofVec3f& newpos) {

    vector< ofVec3f >::iterator it = _hanlders.begin();
    vector< ofVec3f >::iterator ite = _hanlders.end();
    uint8_t i = 0;
    for (; it != ite; ++it) {
        if (&(*it) == handler) {
            set_handler(i, newpos);
            break;
        }
        ++i;
    }

}

void mapable::set_handler(uint32_t id, const ofVec3f& newpos) {

    if (id >= _hanlders.size()) {
        return;
    }

    ofVec3f delta;
    float ampl0;
    float angl0;
    float ampl1;
    float angl1;

    if (id < 2) {
        ofVec3f dir = (_hanlders[1] - _hanlders[0]);
        dir.normalize();
        float da = atan2(dir.y, dir.x);
        delta = (_hanlders[2] - _hanlders[0]);
        ampl0 = delta.length();
        delta.normalize();
        angl0 = atan2(delta.y, delta.x);
        angl0 -= da;
        delta = (_hanlders[3] - _hanlders[1]);
        ampl1 = delta.length();
        delta.normalize();
        angl1 = atan2(delta.y, delta.x);
        angl1 -= da;
    }
    _hanlders[id] = newpos;
    if (id < 2) {
        ofVec3f dir = (_hanlders[1] - _hanlders[0]);
        dir.normalize();
        angl0 += atan2(dir.y, dir.x);
        angl1 += atan2(dir.y, dir.x);
        _hanlders[2] = _hanlders[0] + ofVec3f(
                cos(angl0) * ampl0,
                sin(angl0) * ampl0,
                0);
        _hanlders[3] = _hanlders[1] + ofVec3f(
                cos(angl1) * ampl1,
                sin(angl1) * ampl1,
                0);
    }
    pack();

}

void mapable::gl_color() const {

    glColor4f(_rgba.x, _rgba.y, _rgba.z, _rgba.w);

}

void mapable::draw() {

    if (_rgba.w <= 0) {
        return;
    }

    if (_tweener.begin()) {
        if (_texture) {
            shader_tweener::shader()->setUniform1f("tex0weight", 1);
            shader_tweener::shader()->setUniform2f("tex0size",  
                    _texture->getWidth(),
                    _texture->getHeight()
                    );
            shader_tweener::shader()->setUniformTexture("tex0", 
                    _texture->getTexture(), 0);
        } else {        
            shader_tweener::shader()->setUniform1f("tex0weight", 0);
        }
        shader_tweener::shader()->setUniform4f("rgba", 
                _rgba.x, _rgba.y, _rgba.z, _rgba.w);
        _mesh.draw();
        _tweener.end();
    } else {
        glColor4f(_rgba.x, _rgba.y, _rgba.z, _rgba.w);
        _mesh.draw();
    }

}

void mapable::perimeter() {
    
    glColor4f(1, 1, 1, 0.5);
    ofNoFill();
    ofBeginShape();
    for ( uint8_t i = 0; i < 4; ++i ) {
        ofVertex( _corners[i].x, _corners[i].y, _corners[i].z );
    }
    ofEndShape(true);
    
}

void mapable::debug() const {

    glColor4f(1, 1, 1, 1);
    _mesh.drawWireframe();

}

void mapable::load(const std::string& confname, bool invert) {
    _tweener.load(confname, invert);
}

void mapable::load(const size_t& confid, bool invert) {
    _tweener.load(confid, invert);
}

void mapable::force(const float& v) {
    _tweener.force(v);
}

void mapable::force_min0(const float& v) {
    _tweener.force_min0(v);
}

void mapable::force_min1(const float& v) {
    _tweener.force_min1(v);
}

