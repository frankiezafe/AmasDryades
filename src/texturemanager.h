/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   texture_manager.h
 * Author: frankiezafe
 *
 * Created on June 17, 2018, 4:58 PM
 */

#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include "common.h"

class texturemanager {
    
public:
    
    static texturemanager* get();
    
    static void load( std::string name, std::string path );
    
    static ofImage* get( std::string name );
    
    static void purge();
    
    static std::string json();
    
private:
    
    texturemanager();
    
    virtual ~texturemanager();
    
    std::map< std::string, ofImage* > texture_map;
    std::map< std::string, std::string > path_map;

};

#endif /* TEXTURE_MANAGER_H */

