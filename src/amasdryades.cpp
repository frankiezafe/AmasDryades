#include "amasdryades.h"
#include "ofxOscReceiver.h"
#include "columnmanager.h"

amasdryades::amasdryades() :
ofBaseApp(),
_edit_mode_enabled(false),
_debug_mode_enabled(false),
_add_mode_enabled(false),
_mapable_sel(0),
_mapable_handler(0) {

    std::cout << 
            STD_HR << std::endl <<
            STD_HR << std::endl <<
            "Amas Dryades, v0.1" << std::endl <<
            "how to use:" << std::endl <<
            STD_HR << std::endl <<
            "\t" << "'e' to toggle edit mode" << std::endl <<
            "\t" << "'d' to toggle debug of meshes" << std::endl <<
            "\t" << "'a' to add lines" << std::endl <<
            "\t" << "click on line to select & edit" << std::endl <<
            "\t" << "'del' to delete selected line" << std::endl <<
            "\t" << "'s' to save config" << std::endl <<
            "\t" << "'c' to show all lines and disable automation" << std::endl <<
            "\t" << "'x' to hide all lines and enable automation" << std::endl <<
            "\t" << "'r' to reload line shader and its config" << std::endl <<
            STD_HR << std::endl;
            
}

amasdryades::~amasdryades() {
    
}

void amasdryades::exit() {

//    _map.save(_map_path);

}

void amasdryades::setup() {

    if (_map_path.empty()) {
        _map_path = "flatmap.json";
    }

    ofEnableNormalizedTexCoords();
    ofHideCursor();

    arduinomanager::get();
    arduinomanager::map(&_map);

    if (_map.load(_map_path)) {
        _oscmgr.init(&_map);
        _colmgr.init(&_map, &_oscmgr);
    }

}

void amasdryades::update() {
    
    arduinomanager::update();
    _oscmgr.update();

    if (_mapable_sel && _oscmgr.mapable_received()) {

        const mapable_data& m = _oscmgr.mapable_info();
        if (m.colA > -1) {
            _mapable_sel->colA(m.colA);
        }
        if (m.colB > -1) {
            _mapable_sel->colB(m.colB);
        }
        if (m.subdivide > -1) {
            _mapable_sel->subdivide(m.subdivide);
        }
        if (m.texture_name.compare(MAPABLE_WRONG_TEXTURE) != 0) {
            _mapable_sel->set_texture(m.texture_name);
        }
        if (m.rgba.size() == 4) {
            _mapable_sel->rgba(ofVec4f(m.rgba[0], m.rgba[1], m.rgba[2], m.rgba[3]));
        }

    }

    if (_oscmgr.column_sync()) {
        if (_oscmgr.ctrl_col_id() == numeric_limits<uint32_t>::max()) {
            _colmgr.activate_all();
        } else {
            _colmgr.highlight(_oscmgr.ctrl_col_id());
        }
    }

    _colmgr.update();

    _oscmgr.mapable_reset();
    _oscmgr.column_reset();
    _oscmgr.timer_reset();

}

void amasdryades::draw() {
    
    ofBackground(0, 0, 0);

    ofVec3f screen_scale(ofGetHeight(), ofGetHeight(), ofGetHeight());

    vector< mapable >& ms = _map.mapables();
    vector< mapable >::iterator itm;
    vector< mapable >::iterator itme = ms.end();

    glColor4f(1, 1, 1, 1);

    glScissor(0, 0, ofGetWidth() / 2, ofGetHeight());
    glEnable(GL_SCISSOR_TEST);
    ofPushMatrix();
    ofScale(screen_scale);
    itm = ms.begin();
    for (; itm != itme; ++itm) {
        if ((*itm).viewport() == 0) {
            (*itm).draw();
            if ( _edit_mode_enabled ) {
                (*itm).perimeter();
            }
        }
    }
    ofPopMatrix();
    glDisable(GL_SCISSOR_TEST);

    glScissor(ofGetWidth() / 2, 0, ofGetWidth() / 2, ofGetHeight());
    glEnable(GL_SCISSOR_TEST);
    ofPushMatrix();
    ofScale(screen_scale);
    itm = ms.begin();
    for (; itm != itme; ++itm) {
        if ((*itm).viewport() == 1) {
            (*itm).draw();
            if ( _edit_mode_enabled ) {
                (*itm).perimeter();
            }
        }
    }
    ofPopMatrix();
    glDisable(GL_SCISSOR_TEST);

    ofNoFill();

    if (_edit_mode_enabled) {

        glColor4f(1, 1, 1, 1);
        if (_mapable_sel && !_mapable_handler) {
            glColor4f(1, 1, 0, 0.8);
        } else if (_mapable_handler) {
            glColor4f(0, 1, 1, 1);
        } else if (_add_mode_enabled && _add_vectors.empty()) {
            glColor4f(1, 0, 1, 1);
        } else if (_add_mode_enabled && !_add_vectors.empty()) {
            glColor4f(1, 0.5, 1, 0.8);
        }
        ofDrawLine(
                ofGetMouseX(), 0,
                ofGetMouseX(), ofGetHeight()
                );
        if (ofGetMouseX() <= ofGetWidth() * 0.5) {
            ofDrawLine(
                    0, ofGetMouseY(),
                    ofGetWidth() * 0.5, ofGetMouseY()
                    );
        } else {
            ofDrawLine(
                    ofGetWidth() * 0.5, ofGetMouseY(),
                    ofGetWidth(), ofGetMouseY()
                    );
        }

        if (_add_mode_enabled && !_add_vectors.empty()) {
            ofPushMatrix();
            ofScale(screen_scale);
            glColor4f(1, 1, 1, 1);
            for (uint32_t i = 0, j = 1; i < _add_vectors.size(); ++i, ++j) {
                if (j < _add_vectors.size() - 1) {
                    ofDrawLine(
                            _add_vectors[i],
                            _add_vectors[j]
                            );
                } else {
                    ofDrawLine(
                            _add_vectors[i],
                            _add_vector_current
                            );
                }
            }
            ofPopMatrix();
        }


        if (_mapable_sel) {

            if (_debug_mode_enabled) {
                ofPushMatrix();
                ofScale(screen_scale);
                _mapable_sel->debug();
                ofPopMatrix();
            }

            ofSetColor(255, 255, 0);
            if (_mapable_handler == &(_mapable_sel->hanlders()[0])) {
                ofSetColor(0, 255, 255);
            }
            ofDrawCircle((*_mapable_sel)[0] * screen_scale, HANDLER_MAIN_RAD);
            ofDrawBitmapString(
                    "a:" + ofToString(_mapable_sel->colA()),
                    (*_mapable_sel)[0] * screen_scale +
                    ofVec3f(HANDLER_MAIN_RAD, -HANDLER_MAIN_RAD, 0)
                    );

            ofSetColor(255, 255, 0);
            if (_mapable_handler == &(_mapable_sel->hanlders()[1])) {
                ofSetColor(0, 255, 255);
            }
            ofDrawCircle((*_mapable_sel)[1] * screen_scale, HANDLER_MAIN_RAD);
            ofDrawBitmapString(
                    "b:" + ofToString(_mapable_sel->colB()),
                    (*_mapable_sel)[1] * screen_scale +
                    ofVec3f(HANDLER_MAIN_RAD, HANDLER_MAIN_RAD * 2, 0)
                    );

            ofSetColor(255, 255, 0);
            if (_mapable_handler == &(_mapable_sel->hanlders()[2])) {
                ofSetColor(0, 255, 255);
            }
            ofDrawCircle((*_mapable_sel)[2] * screen_scale, HANDLER_SECONDARY_RAD);

            ofSetColor(255, 255, 0);
            if (_mapable_handler == &(_mapable_sel->hanlders()[3])) {
                ofSetColor(0, 255, 255);
            }
            ofDrawCircle((*_mapable_sel)[3] * screen_scale, HANDLER_SECONDARY_RAD);
        }

        vector< column >::iterator itc = _map.columns().begin();
        vector< column >::iterator itce = _map.columns().end();
        ofVec2f ref(10, ofGetHeight() - 10);

        for (; itc != itce; ++itc) {

            column& c = (*itc);

            ofNoFill();
            std::stringstream ssc;
            ssc << c.UID();
            if (c.data_received()) {
                ofSetColor(0, 255, 255, 255);
                ssc << "^";
            } else {
                ofSetColor(255, 0, 0, 255);
                ssc << "_";
            }
            ofDrawBitmapString(ssc.str(), ref.x, ref.y - 105);
            ofSetColor(0, 255, 255, 120);
            if (
                    _oscmgr.ctrl_col_enabled() &&
                    _oscmgr.ctrl_col_id() == c.UID()
                    ) {

                vector< mapable* >::iterator itm = c.mapables().begin();
                vector< mapable* >::iterator itme = c.mapables().end();
                for (; itm != itme; ++itm) {
                    ofVec3f p;
                    if (uint32_t((*itm)->colA()) == c.UID()) {
                        p = (*(*itm))[0] * screen_scale;
                    } else if (uint32_t((*itm)->colB()) == c.UID()) {
                        p = (*(*itm))[1] * screen_scale;
                    }
                    ofDrawLine(
                            p.x, p.y, p.z,
                            ref.x, ref.y - 100, 0
                            );
                }

            }

            for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
                const photo_resistor& pr = c[i];
                ofNoFill();
                if (c.data_received()) {
                    ofSetColor(255, 255, 255, 100);
                } else {
                    ofSetColor(255, 255, 255, 50);
                }
                ofDrawRectangle(
                        ref.x, ref.y - 100,
                        10, 100);
                ofFill();
                ofSetColor(255, 0, 0, 255);
                ofDrawRectangle(
                        ref.x, ref.y - 100 * pr.value_norm,
                        8, 100 * pr.value_norm);
                ofSetColor(255, 255, 0, 255);
                ofDrawRectangle(
                        ref.x + 8, ref.y - 100 * pr.multiply,
                        2, 100 * pr.multiply);
                ref.x += 12;
            }

            ref.x += 10;

        }

    }

}

void amasdryades::keyPressed(int key) {

    switch (key) {

        case 'e':
            _edit_mode_enabled = !_edit_mode_enabled;
            if (!_edit_mode_enabled) {
                _mapable_sel = 0;
                _mapable_handler = 0;
            }
            break;

        case 'd':
            if (_edit_mode_enabled) {
                _debug_mode_enabled = !_debug_mode_enabled;
            }
            break;

        case 'a':
            if (_edit_mode_enabled) {
                _add_mode_enabled = !_add_mode_enabled;
                if (_add_mode_enabled) {
                    _add_vectors.clear();
                    _mapable_sel = 0;
                    _mapable_handler = 0;
                }
            }
            break;

        case 's':
            _map.save(_map_path);
            std::cout << "flatmap saved: " << _map_path << std::endl;
            break;

        case 'c':
            _colmgr.activate_all();
            _colmgr.automation( false );
            break;

        case 'x':
            _colmgr.deactivate_all();
            _colmgr.automation( true );
            break;
            
        case 'r':
            _map.load_shader_config();\
            shader_tweener::load_shader( true );
            break;

        case 127:
            if (_mapable_sel) {
                _map.remove_mapable(_mapable_sel);
                _mapable_sel = 0;
                _mapable_handler = 0;
                _colmgr.build_mesh();
            }
            break;

        default:
            std::cout << "keyPressed " << key << std::endl;
            break;

    }

}

void amasdryades::keyReleased(int key) {


    switch (key) {

        default:
            // std::cout << "keyReleased " << key << std::endl;
            break;

    }

}

void amasdryades::grab_handler(int x, int y) {

    float h = ofGetHeight();
    ofVec3f mouse(x / h, y / h, 0);

    float rad_main = HANDLER_MAIN_RAD / h;
    float rad_sec = HANDLER_SECONDARY_RAD / h;

    float min_dist = numeric_limits<float>::max();

    float d = (*_mapable_sel)[0].distance(mouse);
    if (d <= rad_main) {
        min_dist = d;
        _mapable_handler = &(_mapable_sel->hanlders()[0]);
    }
    d = (*_mapable_sel)[1].distance(mouse);
    if (d < min_dist && d <= rad_main) {
        min_dist = d;
        _mapable_handler = &(_mapable_sel->hanlders()[1]);
    }
    d = (*_mapable_sel)[2].distance(mouse);
    if (d < min_dist && d <= rad_sec) {
        min_dist = d;
        _mapable_handler = &(_mapable_sel->hanlders()[2]);
    }
    d = (*_mapable_sel)[3].distance(mouse);
    if (d < min_dist && d <= rad_sec) {
        min_dist = d;
        _mapable_handler = &(_mapable_sel->hanlders()[3]);
    }

    if (min_dist == numeric_limits<float>::max()) {
        _mapable_handler = 0;
    }

}

void amasdryades::mouseMoved(int x, int y) {

    if (_edit_mode_enabled && !_add_mode_enabled && _mapable_sel) {

        grab_handler(x, y);

    } else if (_add_mode_enabled) {

        float h = ofGetHeight();
        ofVec3f v(x / h, y / h, 0);
        _add_vector_current = v;

        if (!_add_vectors.empty()) {

            uint8_t current_viewport = 0;
            if (x > ofGetWidth() * 0.5) {
                current_viewport = 1;
            }
            if (current_viewport != _edition_viewport) {
                _add_vector_current.x = ofGetWidth() * 0.5 / h;
            }

        }

    }

}

void amasdryades::mouseDragged(int x, int y, int button) {


    float h = ofGetHeight();
    ofVec3f v(x / h, y / h, 0);

    if (_mapable_sel && _mapable_handler) {

        v += _mapable_handler_offset;
        _mapable_sel->set_handler(_mapable_handler, v);

    }

}

void amasdryades::mousePressed(int x, int y, int button) {

    if (_edit_mode_enabled) {

        float h = ofGetHeight();
        ofVec3f mouse(x / h, y / h, 0);

        uint8_t current_viewport = 0;
        if (x > ofGetWidth() * 0.5) {
            current_viewport = 1;
        }

        if (!_add_mode_enabled) {


            if (_mapable_sel) {

                grab_handler(x, y);

                if (!_mapable_handler) {
                    _mapable_sel = 0;
                }

                if (_mapable_handler) {
                    _mapable_handler_offset = (*_mapable_handler) - mouse;
                }

            }

            if (!_mapable_sel) {

                vector< mapable >& ms = _map.mapables();
                vector< mapable >::iterator itm = ms.begin();
                vector< mapable >::iterator itme = ms.end();
                for (; itm != itme; ++itm) {
                    if ((*itm).hit(mouse)) {
                        select_mappable(&(*itm));
                    }
                }

            }

        } else {

            if (_add_vectors.empty()) {

                _edition_viewport = current_viewport;
                _add_vectors.push_back(mouse);
                _add_vector_current = mouse;

            } else {

                if (current_viewport != _edition_viewport) {
                    return;
                }

                mapable m(
                        _add_vectors[0],
                        mouse
                        );
                m.load( MAPABLE_SHADER_CONFIG_FULLON );
                m.viewport(_edition_viewport);
                select_mappable(_map.add_mapable(m));
                _add_mode_enabled = false;
                _add_vectors.clear();

            }

        }

    }

}

void amasdryades::mouseReleased(int x, int y, int button) {

    _mapable_handler = 0;

}

void amasdryades::mouseEntered(int x, int y) {

}

void amasdryades::mouseExited(int x, int y) {

}

void amasdryades::windowResized(int w, int h) {

}

void amasdryades::gotMessage(ofMessage msg) {

}

void amasdryades::dragEvent(ofDragInfo dragInfo) {

}

void amasdryades::select_mappable(mapable* m) {

    _mapable_sel = m;
    _oscmgr.ctrl_init(_mapable_sel);

}