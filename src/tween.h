/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   tween.h
 * Author: frankiezafe
 *
 * Created on June 21, 2018, 11:33 AM
 */

#ifndef TWEEN_H
#define TWEEN_H

#include "common.h"

class tween {
public:
    
    tween( );
    
    virtual ~tween( );

    void init( float* value );

    void start( const uint64_t& duration, const float& target );

    void stop( );
    
    void stop( const float& v );

    bool update( );
    
    const bool& active() const {
        return _active;
    }
    
    void autonomous();
    
    const float& std_value() const {
        return _std_value;
    }

private:
    
    bool _autonomous;

    float* _value;
    float _std_value;

    float _init;
    float _target;
    bool _active;

    uint64_t _last_time;
    uint64_t _delta;
    uint32_t _duration;
    uint32_t _timer;

};

#endif /* TWEEN_H */

