/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   tween.cpp
 * Author: frankiezafe
 * 
 * Created on June 21, 2018, 11:33 AM
 */

#include "tween.h"

tween::tween() :
_autonomous(false),
_value(0),
_std_value(0),
_init(0),
_target(0),
_active(false),
_last_time(0),
_delta(0),
_duration(0),
_timer(0) {
}

void tween::autonomous() {

    _autonomous = true;
    _std_value = 0;

}

tween::~tween() {
}

void tween::init(float* value) {

    if (_autonomous) {
        _std_value = (*value);
    } else {
        _value = value;
    }
}

void tween::start(const uint64_t& duration, const float& target) {

    if (!_autonomous && !_value) {
        return;
    }
    
    _duration = duration;
    _target = target;
    _last_time = ofGetElapsedTimeMillis();

    if (_autonomous) {
        _init = _std_value;
    } else {
        _init = float(*_value);
    }
    _timer = 0;

}

void tween::stop() {

    _duration = 0;
    _timer = 0;

}

void tween::stop( const float& v ) {
    
    stop();
    _std_value = v;
    
}

bool tween::update() {

    if ((!_autonomous && !_value) || _timer >= _duration) {
        _active = false;
        return _active;
    }
    
    _active = true;

    uint64_t _now = ofGetElapsedTimeMillis();
    _delta = _now - _last_time;
    _last_time = _now;

    _timer += _delta;
    if (_timer > _duration) {
        _timer = _duration;
    }

    float pc = (_timer * 1.f) / _duration;

    if (_autonomous) {
        _std_value = _init + pc * (_target - _init);
    } else {
        (*_value) = _init + pc * (_target - _init);
    }
    return _active;

}


