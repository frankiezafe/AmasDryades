/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   columnmanager.h
 * Author: frankiezafe
 *
 * Created on June 19, 2018, 7:32 PM
 */

#ifndef COLUMNMANAGER_H
#define COLUMNMANAGER_H

#include "common.h"
#include "column.h"
#include "flatmap.h"
#include "oscmanager.h"
#include "arduinomanager.h"

class columnmanager {
public:

    columnmanager( );

    virtual ~columnmanager( );

    void init( flatmap* m, oscmanager* oscm );

    void update( );

    void build_mesh( );

    void deactivate_all( );

    void activate_all( );
    
    void fade_out();

    void highlight( const uint32_t& i );
    
    column* get_column( const uint32_t& UID );
    
    void automation( bool enabled );

private:

    vector< column >* _columns;
    vector< mapable >* _mapables;
    
    column* _sel_column;

    flatmap* _map;
    oscmanager* _oscmgr;

    columnmanager_status _status;
    
    bool _automation_enabled;

    void goto_state( const columnmanager_status& status );
    
    void column_state( const column_status& status );
    
    void new_connection();

};

#endif /* COLUMNMANAGER_H */