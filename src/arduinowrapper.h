/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   arduinowrapper.h
 * Author: frankiezafe
 *
 * Created on June 18, 2018, 4:31 PM
 */

#ifndef ARDUINOWRAPPER_H
#define ARDUINOWRAPPER_H

#include "common.h"
#include "flatmap.h"
#include "ofArduino.h"
#include "ofxOscSender.h"

class arduinowrapper {
    
public:
    
    arduinowrapper( const arduino_data& data );
    
    ~arduinowrapper();

    void start();

    void configure( const int & version );
    
    void analog( const int & pin );
    
    void update();
    
    void map( flatmap* m );
    
    void max_sender( ofxOscSender* oscsend );
    
    const uint32_t& UID() const {
        return _UID;
    }
    const std::string& path() const {
        return _path;
    }
    const bool& configured() const {
        return _configured;
    }
    
private:

    uint32_t _UID;
    std::string _path;
    
    int _digital_current_index;
    
    /* List of digital pins to be used to power up sensors */
    std::vector< int > _digitals;
    
    /* List of analog pins to be used to read sensors */
    std::vector< int > _analogs;
    
    /* Fast id retrieval of pin number, used in arduinowrapper::analog 
     * - key: analog pin
     * - value: position in _analogs vector
     */
    std::map< int, int > _analog_ids;
    
    /* Flag, indicates witch analog pin has been read.
     * Used to trigger method 'next()' in method 'analog()'.
     * - key: analog pin
     * - value: true if received
     */
    std::map< int, bool > _analog_received;
    
    /* Local storage of analog pins received values.
     * - key: analog pin
     * - value: true if received
     */
    std::map< int, float > _analog_values;
    
    /* For each analog pin, pointer to listening column.
     * Populated when method 'map()' is called.
     * - key: pinID
     * - pointer to column, might be null
     */
    std::map< int, column* > _columns;
    
    ofArduino _ard;
    bool _configured;
    
    flatmap* _map;
    ofxOscSender* _max_sender;
    
    void next();

};


#endif /* ARDUINOWRAPPER_H */

