/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   shadertweener.cpp
 * Author: frankiezafe
 * 
 * Created on June 24, 2018, 7:38 PM
 */

#include "shadertweener.h"

static ofShader* _shader = 0;

static vector< shader_config > _configs;

ofShader* shader_tweener::shader() {
    return _shader;
}

void shader_tweener::load_shader( bool force ) {

    if ( !_shader || force ) {
        
        if ( _shader ) {
            delete _shader;
        }
        _shader = new ofShader();
        if ( !_shader->load(MAPABLE_SHADER_VERT, MAPABLE_SHADER_FRAG) ) {
            delete _shader;
            _shader = 0;
            ofLogError() << "Failed to load shader!" << std::endl; 
        }
        
    }

}

void shader_tweener::clear_conf() {
    _configs.clear();
}

void shader_tweener::push_conf(shader_config& conf) {
    _configs.push_back(conf);
}

shader_tweener::shader_tweener() :
_last_time(0),
_delta(0),
_time(0),
_initialised(false) {

    load_shader();
    
    init();

}

shader_tweener::~shader_tweener() {
}

void shader_tweener::init() {
        
    _tween_timer_multiplier.autonomous();
    _tween_pow0.autonomous();
    _tween_pow1.autonomous();
    _tween_phase0.autonomous();
    _tween_phase1.autonomous();
    _tween_min0.autonomous();
    _tween_min1.autonomous();
    _tween_freq.autonomous();
    
    _last_time = ofGetElapsedTimeMillis();

    _initialised = true;

}

void shader_tweener::load(const std::string& confname, bool invert) {

    vector< shader_config >::iterator it = _configs.begin();
    vector< shader_config >::iterator ite = _configs.end();
    for(; it != ite; ++it) {
        if ( (*it).name.compare( confname ) == 0 ) {
            load( (*it), invert );
        }
    }

}

void shader_tweener::load(const size_t& id, bool invert) {

    if (id <= _configs.size()) {
        load(_configs[id], invert);
    }

}

void shader_tweener::load(const shader_config& conf, bool invert) {

    if (!_shader) {
        return;
    }

    if (!_initialised) {
        init();
    }

    _tween_timer_multiplier.start(
            conf.timer_multiplier_duration,
            conf.timer_multiplier);
    
    if (!invert) {
        _tween_pow0.start(conf.pow0_duration, conf.pow0);
        _tween_pow1.start(conf.pow1_duration, conf.pow1);
        _tween_phase0.start(conf.phase0_duration, conf.phase0);
        _tween_phase1.start(conf.phase1_duration, conf.phase1);
        _tween_min0.start(conf.min0_duration, conf.min0);
        _tween_min1.start(conf.min1_duration, conf.min1);
        _tween_freq.start(conf.freq_duration, conf.freq);
    } else {
        _tween_pow0.start(conf.pow1_duration, conf.pow1);
        _tween_pow1.start(conf.pow0_duration, conf.pow0);
        _tween_phase0.start(conf.phase1_duration, conf.phase1);
        _tween_phase1.start(conf.phase0_duration, conf.phase0);
        _tween_min0.start(conf.min1_duration, conf.min1);
        _tween_min1.start(conf.min0_duration, conf.min0);
        _tween_freq.start(conf.freq_duration, -conf.freq);
    }

}

bool shader_tweener::begin() {

    if (!_shader) {
        return false;
    }

    if (!_initialised) {
        init();
    }

    _tween_timer_multiplier.update();
    _tween_pow0.update();
    _tween_pow1.update();
    _tween_phase0.update();
    _tween_phase1.update();
    _tween_min0.update();
    _tween_min1.update();
    _tween_freq.update();
    
    uint64_t _now = ofGetElapsedTimeMillis();
    _delta = _now - _last_time;
    _last_time = _now;
    
    _time += _delta * _tween_timer_multiplier.std_value();
    
    _shader->begin();
    _shader->setUniform1f("timer", _time);
    _shader->setUniform1f("pow0", _tween_pow0.std_value());
    _shader->setUniform1f("pow1", _tween_pow1.std_value());
    _shader->setUniform1f("phase0", _tween_phase0.std_value());
    _shader->setUniform1f("phase1", _tween_phase1.std_value());
    _shader->setUniform1f("min0", _tween_min0.std_value());
    _shader->setUniform1f("min1", _tween_min1.std_value());
    _shader->setUniform1f("freq", _tween_freq.std_value());

    return true;

}

void shader_tweener::end() {

    if (_shader) {
        _shader->end();
    }

}

void shader_tweener::force(const float& v) {

    _tween_timer_multiplier.stop(0);
    _tween_pow0.stop(0);
    _tween_pow1.stop(0);
    _tween_phase0.stop(0);
    _tween_phase1.stop(0);
    _tween_min0.stop(v);
    _tween_min1.stop(v);
    _tween_freq.stop(0);

}

void shader_tweener::force_min0(const float& v) {

    _tween_min0.stop(v);

}

void shader_tweener::force_min1(const float& v) {

    _tween_min1.stop(v);

}

