/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   oscmanager.cpp
 * Author: frankiezafe
 * 
 * Created on June 15, 2018, 4:25 PM
 */

#include "oscmanager.h"
#include "arduinomanager.h"

oscmanager::oscmanager() :
_ctrl_col_id(numeric_limits<uint32_t>::max()),
_ctrl_col_enabled(false),
_column_sync(false),
_column_restart(false),
_map(0),
_column_received(false),
_mapable_received(false),
_starttimer_received(false),
_endtimer_received(false)
{
    mapable_reset(true);
}

oscmanager::~oscmanager() {
}

void oscmanager::mapable_reset(bool force) {

    if (!force && !_mapable_received) {
        return;
    }

    _mapable.colA = -1;
    _mapable.colB = -1;
    _mapable.subdivide = -1;
    _mapable.texture_name = MAPABLE_WRONG_TEXTURE;
    _mapable.rgba.clear();
    _mapable_received = false;

}

void oscmanager::column_reset() {

    _column_sync = false;
    _column_restart = false;

    if (!_column_received) {
        return;
    }

    _column_received = false;

}

void oscmanager::timer_reset() {
    
    _starttimer_received = false;
    _endtimer_received = false;
    
}


void oscmanager::init(flatmap* map) {

    _map = map;

    if (!_map) {
        return;
    }

    std::cout << STD_HR << std::endl <<
            "osc manager initialisation" << std::endl;

    if (!ctrl_osc_receiver.setup(
            map->ctrl_osc_in_port())) {
        std::cout << "\t/!\\failed to start control osc receiver on port " <<
                map->ctrl_osc_in_port() << std::endl;
    } else {
        std::cout << "\tcontrol osc receiver listening to port " <<
                map->ctrl_osc_in_port() << std::endl;
    }

    if (!ctrl_osc_sender.setup(
            map->ctrl_osc_out_ip(),
            map->ctrl_osc_out_port())) {
        std::cout << "\t/!\\failed to start control osc sender on " <<
                map->ctrl_osc_out_ip() << ":" <<
                map->ctrl_osc_out_port() << std::endl;
    } else {
        std::cout << "\tcontrol osc sender sending to " <<
                map->ctrl_osc_out_ip() << ":" <<
                map->ctrl_osc_out_port() << std::endl;
    }

    if (!max_osc_receiver.setup(
            map->max_osc_in_port())) {
        std::cout << "\t/!\\failed to start control max receiver on port " <<
                map->max_osc_in_port() << std::endl;
    } else {
        std::cout << "\tmax osc receiver listening to port " <<
                map->max_osc_in_port() << std::endl;
    }

    if (!max_osc_sender.setup(
            map->max_osc_out_ip(),
            map->max_osc_out_port())) {
        std::cout << "\t/!\\failed to start max osc sender on " <<
                map->max_osc_out_ip() << ":" <<
                map->max_osc_out_port() << std::endl;
    } else {

        std::cout << "\tmaxosc sender sending to " <<
                map->max_osc_out_ip() << ":" <<
                map->max_osc_out_port() << std::endl;

        arduinomanager::max_sender(&max_osc_sender);

    }
    
    std::cout << STD_HR << std::endl;

}

void oscmanager::update_ctrl() {

    while (ctrl_osc_receiver.hasWaitingMessages()) {

        ofxOscMessage m;
        ctrl_osc_receiver.getNextMessage(m);

        std::string addr = m.getAddress();
        std::string type = m.getTypeString();
        if (_map) {

            if (addr.compare("/mappable/colA") == 0) {

                if (type.compare("i") == 0) {
                    _mapable.colA = m.getArgAsInt(0);
                    _mapable_received = true;
                }

            } else if (addr.compare("/mappable/colB") == 0) {

                if (type.compare("i") == 0) {
                    _mapable.colB = m.getArgAsInt(0);
                    _mapable_received = true;
                }

            } else if (addr.compare("/mappable/subdivide") == 0) {

                if (type.compare("i") == 0) {
                    _mapable.subdivide = m.getArgAsInt(0);
                    _mapable_received = true;
                }

            } else if (addr.compare("/mappable/texture") == 0) {

                if (type.compare("s") == 0) {
                    _mapable.texture_name = m.getArgAsString(0);
                    _mapable_received = true;
                }

            } else if (addr.compare("/mappable/rgba") == 0) {

                if (m.getNumArgs() == 4) {
                    _mapable.rgba.resize(4);
                    _mapable.rgba[0] = m.getArgAsFloat(0);
                    _mapable.rgba[1] = m.getArgAsFloat(1);
                    _mapable.rgba[2] = m.getArgAsFloat(2);
                    _mapable.rgba[3] = m.getArgAsFloat(3);
                    _mapable_received = true;
                }

            } else if (addr.compare("/win/w") == 0) {

                if (type.compare("i") == 0 || type.compare("f") == 0) {
                    _map->window().width(m.getArgAsInt(0));
                }

            } else if (addr.compare("/win/h") == 0) {

                if (type.compare("i") == 0 || type.compare("f") == 0) {
                    _map->window().height(m.getArgAsInt(0));
                }

            } else if (addr.compare("/win/x") == 0) {

                if (type.compare("i") == 0 || type.compare("f") == 0) {
                    _map->window().offset_x(m.getArgAsInt(0));
                }

            } else if (addr.compare("/win/y") == 0) {

                if (type.compare("i") == 0 || type.compare("f") == 0) {
                    _map->window().offset_y(m.getArgAsInt(0));
                }

            } else if (addr.compare("/sync") == 0) {

                ofxOscMessage syncm;
                syncm.setAddress("/sync");
                syncm.addInt32Arg(_map->window().width());
                syncm.addInt32Arg(_map->window().height());
                syncm.addInt32Arg(_map->window().offset_x());
                syncm.addInt32Arg(_map->window().offset_y());
                ctrl_osc_sender.sendMessage(syncm, false);

            } else if (addr.compare("/sync_col") == 0) {

                if (type.compare("i") == 0 || type.compare("f") == 0) {

                    uint32_t cid = m.getArgAsInt(0);
                    column* c = _map->get_column_id(cid);

                    if (c) {

                        _ctrl_col_id = cid;
                        _column_sync = true;
                        _ctrl_col_enabled = true;

                        ofxOscMessage syncm;
                        syncm.setAddress("/sync_col");
                        syncm.addInt32Arg(c->UID());
                        syncm.addInt32Arg(c->arduino_id());
                        syncm.addInt32Arg(c->analog_pin());
                        for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
                            syncm.addFloatArg((*c)[i].min);
                            syncm.addFloatArg((*c)[i].max);
                        }
                        ctrl_osc_sender.sendMessage(syncm, false);

                    } else {

                        _ctrl_col_id = numeric_limits<uint32_t>::max();
                        _column_sync = true;
                        _ctrl_col_enabled = false;

                        ofxOscMessage syncm;
                        syncm.setAddress("/sync_col_error");
                        ctrl_osc_sender.sendMessage(syncm, false);

                    }

                }

            } else if (addr.compare("/col_arduino") == 0) {

                if (type.compare("ii") == 0) {

                    uint32_t cid = m.getArgAsInt(0);
                    column* c = _map->get_column_id(cid);

                    if (c) {
                        uint16_t p = m.getArgAsInt(1);
                        c->arduino_id(p);
                        arduinomanager::map(_map);
                        _column_received = true;
                    }

                }

            } else if (addr.compare("/col_analog") == 0) {

                if (type.compare("ii") == 0) {

                    uint32_t cid = m.getArgAsInt(0);
                    column* c = _map->get_column_id(cid);

                    if (c) {
                        uint16_t p = m.getArgAsInt(1);
                        c->analog_pin(p);
                        arduinomanager::map(_map);
                        _column_received = true;
                    }

                }

            } else if (addr.compare("/col_min") == 0) {

                if (type.compare("iif") == 0) {

                    uint32_t cid = m.getArgAsInt(0);
                    column* c = _map->get_column_id(cid);

                    if (c) {
                        uint16_t sid = m.getArgAsInt(1);
                        float v = m.getArgAsFloat(2);
                        c->sensor_min(sid, v);
                        _column_received = true;
                    }

                }

            } else if (addr.compare("/col_max") == 0) {

                if (type.compare("iif") == 0) {

                    uint32_t cid = m.getArgAsInt(0);
                    column* c = _map->get_column_id(cid);

                    if (c) {
                        uint16_t sid = m.getArgAsInt(1);
                        float v = m.getArgAsFloat(2);
                        c->sensor_max(sid, v);
                        _column_received = true;
                    }

                }

            } else if (addr.compare("/col_simul") == 0) {

                if (m.getNumArgs() == 3) {

                    arduinomanager::simulate(
                            uint32_t(m.getArgAsInt(0)),
                            uint8_t(m.getArgAsInt(1)),
                            m.getArgAsFloat(2));

                }

            } else if (addr.compare("/col_mult") == 0) {

                if (type.compare("if") == 0 || type.compare("ii") == 0) {

                    uint32_t cid = m.getArgAsInt(0);
                    column* c = _map->get_column_id(cid);

                    if (c) {
                        c->sensor_multiply(m.getArgAsFloat(1));
                    }

                }

            } else if (addr.compare("/col_simul_restart") == 0) {

                _column_restart = true;

            } else if (addr.compare("/shader/tmult") == 0) {
                //                mapable::timer_multiplier( m.getArgAsFloat(0) );
            } else if (addr.compare("/shader/pow0") == 0) {
                //                mapable::pow0( m.getArgAsFloat(0) );
            } else if (addr.compare("/shader/pow1") == 0) {
                //                mapable::pow1( m.getArgAsFloat(0) );
            } else if (addr.compare("/shader/phase0") == 0) {
                //                mapable::phase0( m.getArgAsFloat(0) );
            } else if (addr.compare("/shader/phase1") == 0) {
                //                mapable::phase1( m.getArgAsFloat(0) );
            } else if (addr.compare("/shader/min0") == 0) {
                //                mapable::min0( m.getArgAsFloat(0) );
            } else if (addr.compare("/shader/min1") == 0) {
                //                mapable::min1( m.getArgAsFloat(0) );
            } else if (addr.compare("/shader/freq") == 0) {
                //                mapable::freq( m.getArgAsFloat(0) );
            } else if (addr.compare("/shader/config") == 0) {
                for (size_t i = 0; i < _map->mapables().size(); ++i) {
                    _map->mapables()[i].load(m.getArgAsInt(0), false);
                }
            } else if (addr.compare("/shader/configi") == 0) {
                for (size_t i = 0; i < _map->mapables().size(); ++i) {
                    _map->mapables()[i].load(m.getArgAsInt(0), true);
                }
            }


        }

    }

    if (_ctrl_col_enabled) {

        column* c = _map->get_column_id(_ctrl_col_id);

        if (c) {

            ofxOscMessage syncm;
            syncm.setAddress("/col_sensors");
            for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
                syncm.addFloatArg((*c)[i].value_norm);
            }
            ctrl_osc_sender.sendMessage(syncm, false);

        }

    }

}

void oscmanager::update_max() {

    while (max_osc_receiver.hasWaitingMessages()) {

        ofxOscMessage m;
        max_osc_receiver.getNextMessage(m);

        std::string addr = m.getAddress();
        std::string type = m.getTypeString();
        
        if ( addr.compare( "/startimer" ) == 0 ) {
            _column_restart = true;
            std::cout << "/startimer" << std::endl;
        } else if ( addr.compare( "/endtimer" ) == 0 ) {
            _endtimer_received = true;
            std::cout << "/endtimer" << std::endl;
        }

        std::cout << "max_osc_receiver " <<
                addr << ":" << type << std::endl;

    }

}

void oscmanager::ctrl_init(mapable* m) {

    mapable_reset();

    ofxOscMessage syncm;
    syncm.setAddress("/mapable/init");
    syncm.addIntArg(m->colA());
    syncm.addIntArg(m->colB());
    syncm.addIntArg(m->subdivide());
    syncm.addStringArg(m->texture_name());
    syncm.addFloatArg(m->rgba().x);
    syncm.addFloatArg(m->rgba().y);
    syncm.addFloatArg(m->rgba().z);
    syncm.addFloatArg(m->rgba().w);
    ctrl_osc_sender.sendMessage(syncm, false);

}

void oscmanager::update() {

    update_ctrl();

    update_max();

}
