#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxJSON.h"

#define CTRL_KEY 2

#define STD_HR "*********************************************************"

#define PhotoResistor_min 0.3
#define PhotoResistor_max 0.7
#define PhotoResistor_touched 0.1

#define HANDLER_MAIN_RAD 10.f
#define HANDLER_SECONDARY_RAD 5.f

#define MAPABLE_SUBDIVIDE 10
#define MAPABLE_WRONG_TEXTURE "!__UNDEFINED__!"

#define MAPABLE_SHADER_CONFIG "mapable/line.json"
#define MAPABLE_SHADER_VERT "mapable/line.vert"
#define MAPABLE_SHADER_FRAG "mapable/line.frag"

#define MAPABLE_SHADER_CONFIG_FULLOFF "full_off"
#define MAPABLE_SHADER_CONFIG_SLOWOFF "slow_off"
#define MAPABLE_SHADER_CONFIG_RISING "rising"
#define MAPABLE_SHADER_CONFIG_SLOWON "slow_on"
#define MAPABLE_SHADER_CONFIG_FULLON "full_on"
#define MAPABLE_SHADER_CONFIG_DIRECT "direct"

#define COLUMN_TIME_DOWN 1
#define COLUMN_TIME_UP 10000
#define COLUMN_TIME_FADEOUT 10000

#define ARDUINO_BAUD 57600
//#define ARDUINO_BAUD 9600

struct shader_config {
    std::string name;
    float timer_multiplier;
    float pow0;
    float pow1;
    float phase0;
    float phase1;
    float min0;
    float min1;
    float freq;
    uint32_t timer_multiplier_duration;
    uint32_t pow0_duration;
    uint32_t pow1_duration;
    uint32_t phase0_duration;
    uint32_t phase1_duration;
    uint32_t min0_duration;
    uint32_t min1_duration;
    uint32_t freq_duration;
};

enum column_status {

    COLUMN_DEFAULT,
    COLUMN_LOW,
    COLUMN_HIGH

};

enum columnmanager_status {

    COLMGR_DEFAULT,
    COLMGR_IDLE,
    COLMGR_STARTED

};

class photo_resistor {

public:
    
    bool active;
    float min;
    float max;
    float multiply;
    float value_raw;
    float value_norm;
    
    photo_resistor():
        active(false),
        min(PhotoResistor_min),
        max(PhotoResistor_max),
        multiply(1),
        value_raw(0),
        value_norm(0)
    {}
    
    void operator = ( const photo_resistor& src ) {
        active = src.active;
        min = src.min;
        max = src.max;
        multiply = src.multiply;
        value_raw = src.value_raw;
        value_norm = src.value_norm;
    }
    
    /* Use this to copy photo_resistor CONFIGURATION only */
    void config( const photo_resistor& src ) {
        active = src.active;
        min = src.min;
        max = src.max;
        multiply = src.multiply;
    }
    
    /* Use this to copy photo_resistor VALUES only */
    void values( const photo_resistor& src ) {
        value_raw = src.value_raw;
        value_norm = src.value_norm;
    }
    
};

struct column_data {
    uint32_t UID;
    uint16_t arduino_id;
    uint16_t analog_pin;
    vector<photo_resistor> sensors;
};

struct mapable_data {
    int colA;
    int colB;
    uint8_t viewport;
    int subdivide;
    std::string texture_name;
    vector<float> rgba;
    vector<float> uv;
    vector<float> handlers;
};

struct arduino_data {
    uint32_t UID;
    std::string path;
    vector<int> digitals;
    vector<int> analogs;
};

typedef std::vector< arduino_data > arduino_data_vector;
typedef arduino_data_vector::iterator arduino_data_iterator;


class window_interface {
public:

    window_interface( ) {
        refresh( );
    }

    void refresh( ) {
        _width = ofGetWindowWidth( );
        _height = ofGetWindowHeight( );
        _offset_x = ofGetWindowPositionX( );
        _offset_y = ofGetWindowPositionY( );
    }
    
    const uint32_t& width() const {
        return _width;
    }
    const uint32_t& height() const {
        return _height;
    }
    const int& offset_x() const {
        return _offset_x;
    }
    const int& offset_y() const {
        return _offset_y;
    }
    
    void set( 
        const uint32_t& w,
        const uint32_t& h,
        const int& x,
        const int& y
    ) {
        _width = w;
        _height = h;
        _offset_x = x;
        _offset_y = y;
        apply();
    }

    void width( uint32_t i ) {
        _width = i;
        apply();
    }

    void height( uint32_t i ) {
        _height = i;
        apply();
    }


    void offset_x( int i ) {
        _offset_x = i;
        apply();
    }


    void offset_y( int i ) {
        _offset_y = i;
        apply();
    }


private:

    uint32_t _width;
    uint32_t _height;
    int _offset_x;
    int _offset_y;

    void apply( ) {
        ofSetWindowPosition( _offset_x, _offset_y );
        ofSetWindowShape( _width, _height );
    }

};