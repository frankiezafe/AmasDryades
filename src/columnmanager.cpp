/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   columnmanager.cpp
 * Author: frankiezafe
 * 
 * Created on June 19, 2018, 7:32 PM
 */

#include "columnmanager.h"

columnmanager::columnmanager() :
_columns(0),
_mapables(0),
_sel_column(0),
_map(0),
_oscmgr(0),
_status(COLMGR_DEFAULT),
_automation_enabled(true) {
}

columnmanager::~columnmanager() {
}

void columnmanager::init(flatmap* m, oscmanager* oscm) {

    _map = m;
    _oscmgr = oscm;

    _columns = &_map->columns();
    _mapables = &_map->mapables();

    build_mesh();
    
    goto_state(COLMGR_IDLE);

}

void columnmanager::automation(bool enabled) {
    _automation_enabled = enabled;
}

void columnmanager::update() {

    if (!_map || !_oscmgr || !_automation_enabled) {
        return;
    }

    _sel_column = 0;

    if (
            _oscmgr->column_received() ||
            _oscmgr->mapable_received()
            ) {
        build_mesh();
    }

    if (_oscmgr->column_restart()) {

//        goto_state(COLMGR_IDLE);

    } else if ( _oscmgr->endtimer_received() ) {
        
        goto_state(COLMGR_IDLE);
        
    }

    if (_status == COLMGR_IDLE) {

        bool touched = false;

        vector< column >::iterator itc = _columns->begin();
        vector< column >::iterator itce = _columns->end();
        float highest = 0;
        for (; itc != itce; ++itc) {
            column& c = (*itc);
            if ((*itc).touched()) {
                float s = c.highest_sensor();
                if (highest < s) {
                    highest = s;
                    _sel_column = &(*itc);
                    touched = true;
                }
            }
        }

        if (touched && _sel_column) {
            goto_state(COLMGR_STARTED);
            new_connection();
        }

    } else if (_status == COLMGR_STARTED) {

        vector< column >::iterator itc = _columns->begin();
        vector< column >::iterator itce = _columns->end();
        for (; itc != itce; ++itc) {
            column& c = (*itc);
            if ((*itc).touched()) {
                _sel_column = &(*itc);
                new_connection();
            }
        }

    }

}

void columnmanager::new_connection() {

    if (!_sel_column) {
        return;
    }

    mapable* m = _sel_column->pick_mapable();
    if (m) {
        uint32_t oid = m->colB();
        if (oid == _sel_column->UID()) {
            oid = m->colA();
        }
        column* other = get_column(oid);
        if (other) {
            other->entry_mapable(m);
            other->tween_multiply(COLUMN_TIME_UP, 1);
        }
    }

}

column* columnmanager::get_column(const uint32_t& UID) {

    vector< column >::iterator itc = _columns->begin();
    vector< column >::iterator itce = _columns->end();
    for (; itc != itce; ++itc) {
        if ((*itc).UID() == UID) {
            return &(*itc);
        }
    }
    return 0;

}

void columnmanager::goto_state(const columnmanager_status& status) {

    switch (status) {

        case COLMGR_IDLE:
            _status = COLMGR_IDLE;
            deactivate_all();
            column_state(COLUMN_HIGH);
            break;

        case COLMGR_STARTED:
            _status = COLMGR_STARTED;
            column_state(COLUMN_LOW);
            break;

        default:
            activate_all();
            break;

    }

}

void columnmanager::column_state(const column_status& status) {

    vector< column >::iterator itc = _columns->begin();
    vector< column >::iterator itce = _columns->end();

    for (; itc != itce; ++itc) {

        column& c = (*itc);

        switch (status) {

            case COLUMN_DEFAULT:
                break;

            case COLUMN_LOW:
                if (_status == COLMGR_STARTED && _sel_column != &(*itc)) {
                    c.deactivate();
                }
                break;

            case COLUMN_HIGH:
                c.activate();
                break;

            default:
                break;

        }

    }

}

void columnmanager::deactivate_all() {

    vector< mapable >::iterator itm = _mapables->begin();
    vector< mapable >::iterator itme = _mapables->end();
    for (; itm != itme; ++itm) {
        (*itm).load(MAPABLE_SHADER_CONFIG_FULLOFF);
    }

    vector< column >::iterator itc = _columns->begin();
    vector< column >::iterator itce = _columns->end();
    for (; itc != itce; ++itc) {
        (*itc).deactivate();
    }

}

void columnmanager::activate_all() {

    vector< column >::iterator itc = _columns->begin();
    vector< column >::iterator itce = _columns->end();
    for (; itc != itce; ++itc) {
        (*itc).activate();
    }

    vector< mapable >::iterator itm = _mapables->begin();
    vector< mapable >::iterator itme = _mapables->end();
    for (; itm != itme; ++itm) {
        (*itm).load(MAPABLE_SHADER_CONFIG_FULLON);
    }

}

void columnmanager::fade_out() {

    vector< column >::iterator itc = _columns->begin();
    vector< column >::iterator itce = _columns->end();
    for (; itc != itce; ++itc) {
        (*itc).activate();
        (*itc).tween_multiply(COLUMN_TIME_FADEOUT, 1);
    }

    vector< mapable >::iterator itm = _mapables->begin();
    vector< mapable >::iterator itme = _mapables->end();
    for (; itm != itme; ++itm) {
        (*itm).load(MAPABLE_SHADER_CONFIG_SLOWOFF);
    }

}

void columnmanager::highlight(const uint32_t& i) {

    deactivate_all();

    vector< column >::iterator itc = _columns->begin();
    vector< column >::iterator itce = _columns->end();
    for (; itc != itce; ++itc) {
        if ((*itc).UID() == i) {
            (*itc).activate();
            return;
        }
    }

}

void columnmanager::build_mesh() {

    vector< column >::iterator itc = _columns->begin();
    vector< column >::iterator itce = _columns->end();

    vector< mapable >::iterator itm;
    vector< mapable >::iterator itme = _mapables->end();

    for (; itc != itce; ++itc) {

        column& c = (*itc);
        c.clear_mapables();

        for (itm = _mapables->begin(); itm != itme; ++itm) {

            mapable& m = (*itm);
            uint32_t a = m.colA();
            uint32_t b = m.colB();

            if (
                    (a == c.UID() && b != c.UID()) || 
                    (a != c.UID() && b == c.UID())
                    ) {
                c.add_connector(&(*itm));
            } else if ( a == c.UID() && b == c.UID() ) {
                c.add_direct(&(*itm));
            }

        }

    }

}