/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   mappable.h
 * Author: frankiezafe
 *
 * Created on June 17, 2018, 3:53 PM
 */

#ifndef MAPPABLE_H
#define MAPPABLE_H

#include "common.h"
#include "texturemanager.h"
#include "shadertweener.h"

class mapable {
public:

    mapable( );

    mapable( const ofVec3f& pt0, const ofVec3f& pt1 );

    mapable( const mapable_data& src );

    virtual ~mapable( );

    std::string json( );

    void refresh( );

    void set_texture( std::string name );

    void set_handler( ofVec3f* handler, const ofVec3f& newpos );

    void set_handler( uint32_t id, const ofVec3f& newpos );

    void colA( const uint32_t& i ) {
        _colA = i;
    }

    void colB( const uint32_t& i ) {
        _colB = i;
    }

    void subdivide( const uint8_t& i ) {
        _subdivide = i;
        pack( );
    }

    void rgba( const ofVec4f& i ) {
        _rgba = i;
    }

    void viewport( const uint8_t& i ) {
        _viewport = i;
    }

    void alpha( const float& a ) {
        _rgba.w = a;
    }

    void draw( );
    
    void perimeter();

    void debug( ) const;

    void gl_color( ) const;

    bool hit( const ofVec3f& pt );

    const ofVec4f& rgba( ) const {
        return _rgba;
    }

    const uint8_t& viewport( ) const {
        return _viewport;
    }

    const int subdivide( ) const {
        return int(_subdivide );
    }

    const int colA( ) const {
        return int(_colA );
    }

    const int colB( ) const {
        return int(_colB );
    }

    const ofVec3f& center( ) const {
        return _center;
    }

    const std::string& texture_name( ) const {
        return _texture_name;
    }

    vector< ofVec3f >& hanlders( ) {
        return _hanlders;
    }

    const ofVec3f& operator[]( uint8_t i ) const {
        return _hanlders[i];
    }

    void load( const std::string& confname, bool invert = false );

    void load( const size_t& confid, bool invert = false );

    void force( const float& v );

    void force_min0( const float& v );

    void force_min1( const float& v );

protected:

    uint32_t _colA;
    uint32_t _colB;
    uint8_t _viewport;
    uint8_t _subdivide;
    ofVec4f _rgba;
    std::string _texture_name;
    ofImage* _texture;

    shader_tweener _tweener;

    bool _generate_hanlders;
    vector< ofVec3f > _hanlders;
    vector< ofVec3f > _perpendiculars;
    vector< ofVec3f > _corners;
    vector< ofVec2f > uv_coords;
    ofVec3f _center;

    ofVboMesh _mesh;

    void init( );

    void pack( );

private:

};

#endif /* MAPPABLE_H */

