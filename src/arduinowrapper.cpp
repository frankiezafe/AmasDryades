/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   arduinowrapper.cpp
 * Author: frankiezafe
 * 
 * Created on June 18, 2018, 4:31 PM
 */

#include "arduinowrapper.h"

arduinowrapper::arduinowrapper(const arduino_data& data) :
_UID(data.UID),
_path(data.path),
_digital_current_index(-1),
_digitals(data.digitals),
_analogs(data.analogs),
_configured(false),
_map(0),
_max_sender(0) {
}

arduinowrapper::~arduinowrapper() {
}

void arduinowrapper::start() {
    _ard.connect(_path, ARDUINO_BAUD);
    ofAddListener(_ard.EInitialized, this, &arduinowrapper::configure);
}

void arduinowrapper::configure(const int & version) {

    ofRemoveListener(_ard.EInitialized, this, &arduinowrapper::configure);
    std::cout << "arduino configured " <<
            _ard.getFirmwareName() << std::endl;

    for (size_t i = 0; i < _digitals.size(); ++i) {
        _ard.sendDigitalPinMode(_digitals[i], ARD_OUTPUT);
        _ard.sendDigital(_digitals[i], ARD_LOW, true);
    }
    for (size_t i = 0; i < _analogs.size(); ++i) {
        _analog_ids[_analogs[i]] = i;
        _analog_received[_analogs[i]] = false;
        _ard.sendAnalogPinReporting(_analogs[i], ARD_ANALOG);
    }

    ofAddListener(_ard.EAnalogPinChanged, this, &arduinowrapper::analog);

    _configured = true;

}

void arduinowrapper::analog(const int & pin) {

    if (_digital_current_index < 0) {
        // no idea what pin is generating this value
        return;
    }

    int v = _ard.getAnalog(pin);
    _analog_received[pin] = true;
    _analog_values[pin] = v;

    if (_columns[ pin ]) {
        float n = _columns[ pin ]->update_thread_sensor(
                _digital_current_index,
                v / 1024.f);
        // n to send via OSC!!!!
        if ( _max_sender ) {
            int cid = _UID * 24 + pin * 4 + _digital_current_index;
            std::stringstream ss;
            ss << "/capteur" << cid;
            ofxOscMessage msg;
            msg.setAddress( ss.str() );
            msg.addInt32Arg( int( n * 1024 ) );
            _max_sender->sendMessage( msg );
        }
    }

    next();

}

void arduinowrapper::next() {

    bool all_received = true;
    for (size_t i = 0; i < _analogs.size(); ++i) {
        if (!_analog_received[_analogs[i]]) {
            all_received = false;
        }
    }
    if (all_received) {

        _ard.sendDigital(_digitals[_digital_current_index], ARD_LOW, true);
        ++_digital_current_index;
        _digital_current_index %= _digitals.size();
        _ard.sendDigital(_digitals[_digital_current_index], ARD_HIGH, true);

        for (size_t i = 0; i < _analogs.size(); ++i) {
            _analog_received[_analogs[i]] = false;
        }

    }

}

void arduinowrapper::update() {

    _ard.update();

    if (!_configured) {
        return;
    }

    if (_digital_current_index == -1) {
        _digital_current_index = 0;
        _ard.sendDigital(_digitals[_digital_current_index], ARD_HIGH, true);
    }

}

void arduinowrapper::map(flatmap* m) {

    _columns.clear();
    _map = m;

    if (!_map) {
        return;
    }

    for (size_t i = 0; i < _analogs.size(); ++i) {
        _columns[ _analogs[i] ] = _map->get_arduino_column(
                _UID, _analogs[i]);
        if (_columns[ _analogs[i] ]) {
            std::cout << "arduino " << _UID <<
                    " connected with column " <<
                    _columns[ _analogs[i] ]->UID() <<
                    std::endl;
        }
    }

}

void arduinowrapper::max_sender( ofxOscSender* oscsend ) {
    
    _max_sender = oscsend;
    
}