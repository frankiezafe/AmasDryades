/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   arduinomanager.h
 * Author: frankiezafe
 *
 * Created on June 18, 2018, 8:02 AM
 */

#ifndef ARDUINOMANAGER_H
#define ARDUINOMANAGER_H

#include "common.h"
#include "flatmap.h"
#include "arduinowrapper.h"

class arduinomanager : public ofThread {
public:

    static arduinomanager* get( );

    static void push( arduino_data& data );

    static void push( arduino_data_vector& list );

    static void map( flatmap* m );

    static void max_sender( ofxOscSender* oscsend );

    static void simulate(
                    const uint32_t& col_id,
                    const uint8_t& sensor_id,
                    const float& v );

    /* Thread save update, to do before accessing columns
     * in the display thread.
     * This will call the method column.sync() on all columns.
     */
    static void update( );

protected:

    void threadedFunction( );

private:

    arduinomanager( );

    virtual ~arduinomanager( );

    int get_device_id( const std::string& path );

    void boot_arduino( const arduino_data& adata );

    void parse_connection_requests( );

    ofSerial _serial;

    uint64_t _now;
    uint64_t _lasttime;
    uint64_t _delta;

    uint64_t _connection_request_wait;
    uint64_t _connection_request_interval;
    bool _connection_request_check;

    arduino_data_vector _connection_requests;
    arduino_data_vector _active_serials;
    std::map< uint32_t, arduinowrapper* > _arduinos;

    flatmap* _map;
    ofxOscSender* _max_sender;

};

#endif /* ARDUINOMANAGER_H */

