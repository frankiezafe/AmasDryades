/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   oscmanager.h
 * Author: frankiezafe
 *
 * Created on June 15, 2018, 4:25 PM
 */

#ifndef OSCMANAGER_H
#define OSCMANAGER_H

#include "common.h" 
#include "flatmap.h"

class oscmanager {

public:
    
    oscmanager( );
    virtual ~oscmanager( );
    
    void init( flatmap* map );
    
    void update();
    
    void ctrl_init( mapable* m );
    
    const uint32_t& ctrl_col_id() const {
        return _ctrl_col_id;
    }
    const bool& ctrl_col_enabled() const {
        return _ctrl_col_enabled;
    }
    const bool& column_sync() const {
        return _column_sync;
    }
    const bool& column_restart() const {
        return _column_restart;
    }
    const bool& column_received() const {
        return _column_received;
    }
    const bool& mapable_received() const {
        return _mapable_received;
    }
    const bool& starttimer_received() const {
        return _starttimer_received;
    }
    const bool& endtimer_received() const {
        return _endtimer_received;
    }
    const mapable_data& mapable_info() const {
        return _mapable;
    }
    
    void mapable_reset( bool force = false );
    
    void column_reset();
    
    void timer_reset();
    
private:
    
    ofxOscReceiver ctrl_osc_receiver;
    ofxOscSender ctrl_osc_sender;
    
    ofxOscReceiver max_osc_receiver;
    ofxOscSender max_osc_sender;
    
    uint32_t _ctrl_col_id;
    bool _ctrl_col_enabled;
    bool _column_sync;
    bool _column_restart;
    int _mvt_received;
    
    flatmap* _map;
    
    bool _column_received;
    bool _mapable_received;
    bool _starttimer_received;
    bool _endtimer_received;
//    bool _reset_received;
    mapable_data _mapable;
    
    void update_ctrl();
    
    void update_max();

};

#endif /* OSCMANAGER_H */

