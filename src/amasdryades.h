#pragma once

#include "common.h"
#include "oscmanager.h"
#include "texturemanager.h"
#include "arduinomanager.h"
#include "columnmanager.h"

class amasdryades : public ofBaseApp {
public:

    amasdryades();
    ~amasdryades();

    void setup();
    void update();
    void draw();
    void exit();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

private:

    std::string _map_path;
    flatmap _map;
    oscmanager _oscmgr;
    columnmanager _colmgr;

    bool _edit_mode_enabled;
    bool _debug_mode_enabled;
    bool _add_mode_enabled;
    mapable* _mapable_sel;
    ofVec3f* _mapable_handler;
    ofVec3f _mapable_handler_offset;
    vector< ofVec3f > _add_vectors;
    uint8_t _edition_viewport;
    ofVec3f _add_vector_current;
            
    void grab_handler(int x, int y);
    
    void select_mappable( mapable* m );


};
