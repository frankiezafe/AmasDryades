/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   shadertweener.h
 * Author: frankiezafe
 *
 * Created on June 24, 2018, 7:38 PM
 */

#ifndef SHADERTWEENER_H
#define SHADERTWEENER_H

#include "common.h"
#include "tween.h"

class shader_tweener {
public:
        
    static ofShader* shader();
    
    static void load_shader( bool force = false );
        
    static void clear_conf();

    static void push_conf(shader_config& conf);

    shader_tweener();

    ~shader_tweener();

    void init();
    
    void load(const std::string& confname, bool invert);
    
    void load(const size_t& confid, bool invert);
    
    void load(const shader_config& conf, bool invert);

    void force(const float& v);
    
    void force_min0(const float& v);
    
    void force_min1(const float& v);
    
    bool begin();
    
    void end();

private:
    
    uint64_t _last_time;
    uint64_t _delta;
    float _time;

    tween _tween_timer_multiplier;
    tween _tween_pow0;
    tween _tween_pow1;
    tween _tween_phase0;
    tween _tween_phase1;
    tween _tween_min0;
    tween _tween_min1;
    tween _tween_freq;
    
    bool _initialised;

};

#endif /* SHADERTWEENER_H */

