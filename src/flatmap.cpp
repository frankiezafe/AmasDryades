/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   flatmap.cpp
 * Author: frankiezafe
 * 
 * Created on June 15, 2018, 3:52 PM
 */

#include "flatmap.h"
#include "arduinomanager.h"

flatmap::flatmap() :
_column_touched_threshold(PhotoResistor_touched) {

    //    _mapables.push_back( mapable() );
    //    _mapables[0].set_texture( "hannibal" );

}

flatmap::~flatmap() {
}

void flatmap::load_shader_config() {
    
    shader_tweener::clear_conf();
    
    std::stringstream ss;
    
    ss << STD_HR << std::endl << "shader configuration loading, should have " <<
            std::endl << "[" <<
            MAPABLE_SHADER_CONFIG_FULLOFF << ", " <<
            MAPABLE_SHADER_CONFIG_SLOWOFF << ", " <<
            MAPABLE_SHADER_CONFIG_RISING << ", " <<
            MAPABLE_SHADER_CONFIG_SLOWON << ", " <<
            MAPABLE_SHADER_CONFIG_FULLON << ", " <<
            MAPABLE_SHADER_CONFIG_DIRECT << "] " << std::endl <<
            "to behave normally" <<
            std::endl << STD_HR <<
            std::endl;
    
    ofxJSONElement ms_config;
    if ( ms_config.open(MAPABLE_SHADER_CONFIG) ) {
        
        for (uint32_t i = 0; i < ms_config["configs"].size(); ++i) {
            
            shader_config sc;
            
            sc.name = ms_config["configs"][i]["name"].asString();
            
            sc.timer_multiplier = ms_config["configs"][i]["timer_multiplier"].asFloat();
            sc.pow0 = ms_config["configs"][i]["pow0"].asFloat();
            sc.pow1 = ms_config["configs"][i]["pow1"].asFloat();
            sc.phase0 = ms_config["configs"][i]["phase0"].asFloat();
            sc.phase1 = ms_config["configs"][i]["phase1"].asFloat();
            sc.min0 = ms_config["configs"][i]["min0"].asFloat();
            sc.min1 = ms_config["configs"][i]["min1"].asFloat();
            sc.freq = ms_config["configs"][i]["freq"].asFloat();
            
            sc.timer_multiplier_duration = ms_config["configs"][i]["timer_multiplier_duration"].asUInt();
            sc.pow0_duration = ms_config["configs"][i]["pow0_duration"].asUInt();
            sc.pow1_duration = ms_config["configs"][i]["pow1_duration"].asUInt();
            sc.phase0_duration = ms_config["configs"][i]["phase0_duration"].asUInt();
            sc.phase1_duration = ms_config["configs"][i]["phase1_duration"].asUInt();
            sc.min0_duration = ms_config["configs"][i]["min0_duration"].asUInt();
            sc.min1_duration = ms_config["configs"][i]["min1_duration"].asUInt();
            sc.freq_duration = ms_config["configs"][i]["freq_duration"].asUInt();
            
//            std::cout << "shader_config:" << std::endl <<
//                    "\t" << "timer_multiplier:" << sc.timer_multiplier << std::endl <<
//                    "\t" << "timer_multiplier_duration:" << sc.timer_multiplier_duration << std::endl <<
//                    "\t" << "pow0:" << sc.pow0 << std::endl <<
//                    "\t" << "pow0_duration:" << sc.pow0_duration << std::endl;
            
            shader_tweener::push_conf( sc );
            
            ss << "\tshader config '" << sc.name << "' loaded" << std::endl;
            
        }
        
    } else {
        
        ss << "! failed to load shader configs!" << std::endl;
    }
    
    ss << STD_HR << std::endl;
    
    std::cout << ss.str() << std::endl;
    
} 

bool flatmap::load(std::string path) {

    load_shader_config();
    
    json_path = path;
    if (!json.open(path)) {
        return false;
    }

    _columns.clear();
    _mapables.clear();
    _arduino_data.clear();

    //    ofLogNotice("flatmap::load >> ") << json.getRawString();

    _ctrl_osc_out_ip = json["ctrl_osc_out"]["ip"].asString();
    _ctrl_osc_out_port = json["ctrl_osc_out"]["port"].asInt();
    _ctrl_osc_in_port = json["ctrl_osc_in"]["port"].asInt();

    _max_osc_out_ip = json["max_osc_out"]["ip"].asString();
    _max_osc_out_port = json["max_osc_out"]["port"].asInt();
    _max_osc_in_port = json["max_osc_in"]["port"].asInt();

    _column_touched_threshold = json["column_touched_threshold"].asFloat();

    _window.set(
            json["window"]["width"].asInt(),
            json["window"]["height"].asInt(),
            json["window"]["offset_x"].asInt(),
            json["window"]["offset_y"].asInt()
            );


    for (uint32_t i = 0; i < json["arduino"].size(); ++i) {

        arduino_data ard;
        ard.UID = json["arduino"][i]["UID"].asInt();
        ard.path = json["arduino"][i]["path"].asString();
        for (uint32_t j = 0; j < json["arduino"][i]["digitals"].size(); ++j) {
            ard.digitals.push_back(
                    json["arduino"][i]["digitals"][j].asInt()
                    );
        }
        for (uint32_t j = 0; j < json["arduino"][i]["analogs"].size(); ++j) {
            ard.analogs.push_back(
                    json["arduino"][i]["analogs"][j].asInt()
                    );
        }
        _arduino_data.push_back(ard);
        arduinomanager::push(ard);

    }

    for (uint32_t i = 0; i < json["textures"].size(); ++i) {

        texturemanager::load(
                json["textures"][i]["name"].asString(),
                json["textures"][i]["path"].asString()
                );

    }

    for (uint32_t i = 0; i < json["columns"].size(); ++i) {

        column_data cd;
        cd.UID = json["columns"][i]["UID"].asInt64();
        cd.arduino_id = json["columns"][i]["arduino_id"].asInt();
        cd.analog_pin = json["columns"][i]["analog_pin"].asInt();
        for (uint32_t s = 0; s < json["columns"][i]["sensors"].size(); ++s) {
            photo_resistor pr;
            pr.min = json["columns"][i]["sensors"][s]["min"].asFloat();
            pr.max = json["columns"][i]["sensors"][s]["max"].asFloat();
            cd.sensors.push_back(pr);
        }
        column c(cd);
        c.touched_threshold( _column_touched_threshold );
        _columns.push_back(c);
        
    }

    for (uint32_t i = 0; i < json["mapables"].size(); ++i) {

        mapable_data md;
        md.colA = json["mapables"][i]["columns"][0].asInt64();
        md.colB = json["mapables"][i]["columns"][1].asInt64();
        md.viewport = json["mapables"][i]["viewport"].asInt();
        md.subdivide = json["mapables"][i]["subdivide"].asInt();
        md.texture_name = json["mapables"][i]["texture_name"].asString();
        for (uint32_t j = 0; j < json["mapables"][i]["rgba"].size(); ++j) {
            md.rgba.push_back(json["mapables"][i]["rgba"][j].asFloat());
        }
        for (uint32_t j = 0; j < json["mapables"][i]["uv"].size(); ++j) {
            for (uint32_t k = 0; k < json["mapables"][i]["uv"][j].size(); ++k) {
                md.uv.push_back(json["mapables"][i]["uv"][j][k].asFloat());
            }
        }
        for (uint32_t j = 0; j < json["mapables"][i]["handlers"].size(); ++j) {
            for (uint32_t k = 0; k < json["mapables"][i]["handlers"][j].size(); ++k) {
                md.handlers.push_back(json["mapables"][i]["handlers"][j][k].asFloat());
            }
        }
        mapable m(md);
        _mapables.push_back(m);

    }

    // registration of columns in arduinos
    arduinomanager::map(this);

    return true;

}

bool flatmap::save(std::string path) {

    std::string save_path = path;

    std::stringstream content;
    content << "{";
    content << "\"ctrl_osc_in\": { " <<
            "\"port\": " << _ctrl_osc_in_port <<
            "},";
    content << "\"ctrl_osc_out\": { " <<
            "\"ip\": \"" << _ctrl_osc_out_ip << "\"," <<
            "\"port\": " << _ctrl_osc_out_port <<
            "},";
    content << "\"max_osc_in\": { " <<
            "\"port\": " << _max_osc_in_port <<
            "},";
    content << "\"max_osc_out\": { " <<
            "\"ip\": \"" << _max_osc_out_ip << "\"," <<
            "\"port\": " << _max_osc_out_port <<
            "},";
    content << "\"window\": { " <<
            "\"width\": " << _window.width() << "," <<
            "\"height\": " << _window.height() << "," <<
            "\"offset_x\": " << _window.offset_x() << "," <<
            "\"offset_y\": " << _window.offset_y() <<
            "},";

    content << "\"column_touched_threshold\":" <<
            _column_touched_threshold << ",";

    content << "\"arduino\":[";
    arduino_data_iterator ita = _arduino_data.begin();
    arduino_data_iterator itab = _arduino_data.begin();
    arduino_data_iterator itae = _arduino_data.end();
    for (; ita != itae; ++ita) {
        if (ita != itab) {
            content << ", ";
        }
        arduino_data& ad = (*ita);
        content << "{\"UID\":" << ad.UID << "," <<
                "\"path\":\"" << ad.path << "\",";
        content << "\"digitals\":[";
        for (uint8_t i = 0; i < ad.digitals.size(); ++i) {
            if (i > 0) {
                content << ",";
            }
            content << ad.digitals[i];
        }
        content << "],";
        content << "\"analogs\":[";
        for (uint8_t i = 0; i < ad.analogs.size(); ++i) {
            if (i > 0) {
                content << ",";
            }
            content << ad.analogs[i];
        }
        content << "]}";
    }
    content << "],";

    content << texturemanager::json() << ",";

    content << "\"columns\": [";
    vector< column >::iterator itc = _columns.begin();
    vector< column >::iterator itcb = _columns.begin();
    vector< column >::iterator itce = _columns.end();
    for (; itc != itce; ++itc) {
        if (itc != itcb) {
            content << ",";
        }
        content << (*itc).json();
    }
    content << "],";

    content << "\"mapables\": [";
    vector< mapable >::iterator itm = _mapables.begin();
    vector< mapable >::iterator itmb = _mapables.begin();
    vector< mapable >::iterator itme = _mapables.end();
    for (; itm != itme; ++itm) {
        if (itm != itmb) {
            content << ",";
        }
        content << (*itm).json();
    }
    content << "]";

    content << "}";

    std::cout << content.str() << std::endl;

    ofxJSONElement save_json(content.str());

    //    ofLogNotice("flatmap::load >> ") << json.getRawString();

    if (!save_json.save(save_path, true)) {
        return false;
    }

    return true;

}

column* flatmap::get_column_id(const uint32_t& id) {

    vector< column >::iterator itc = _columns.begin();
    vector< column >::iterator itce = _columns.end();
    for (; itc != itce; ++itc) {

        if ((*itc).UID() == id) {
            return &(*itc);
        }

    }

    return 0;

}

column* flatmap::get_arduino_column(const uint16_t& ard_id, const uint16_t& analog) {

    vector< column >::iterator itc = _columns.begin();
    vector< column >::iterator itce = _columns.end();
    for (; itc != itce; ++itc) {

        if (
                (*itc).arduino_id() == ard_id &&
                (*itc).analog_pin() == analog
                ) {
            return &(*itc);
        }

    }

    return 0;

}

mapable* flatmap::get_mapable(const uint32_t& id) {

    if (id >= _mapables.size()) {
        return 0;
    }

    return &(_mapables[id]);

}

mapable* flatmap::add_mapable(mapable& m) {

    _mapables.push_back(m);
    _mapables[ _mapables.size() - 1 ].refresh();
    return get_mapable(_mapables.size() - 1);

}

void flatmap::remove_mapable(mapable* m) {
    
    vector< mapable >::iterator itm = _mapables.begin();
    vector< mapable >::iterator itme = _mapables.end();
    for( ; itm != itme; ++itm ) {
        if ( &(*itm) == m ) {
            _mapables.erase( itm );
            break;
        }
    }

}
