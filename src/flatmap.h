/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   flatmap.h
 * Author: frankiezafe
 *
 * Created on June 15, 2018, 3:52 PM
 */

#include "common.h"
#include "texturemanager.h"
#include "column.h"
#include "mapable.h"

#ifndef FLATMAP_H
#define FLATMAP_H

class flatmap {
public:
    
    flatmap( );
    
    virtual ~flatmap( );
    
    bool load( std::string path );
    
    void load_shader_config();
    
    bool save( std::string path );
    
    column* get_column_id( const uint32_t& id );
    
    column* get_arduino_column( const uint16_t& ard_id, const uint16_t& analog );
    
    mapable* get_mapable( const uint32_t& id );
    
    mapable* add_mapable( mapable& m );
    
    void remove_mapable( mapable* m );
    
    const std::string& ctrl_osc_out_ip() const {
        return _ctrl_osc_out_ip;
    }    
    const uint16_t& ctrl_osc_out_port() const {
        return _ctrl_osc_out_port;
    }
    const uint16_t& ctrl_osc_in_port() const {
        return _ctrl_osc_in_port;
    }
    
    const std::string& max_osc_out_ip() const {
        return _max_osc_out_ip;
    }    
    const uint16_t& max_osc_out_port() const {
        return _max_osc_out_port;
    }
    const uint16_t& max_osc_in_port() const {
        return _max_osc_in_port;
    }
    
    window_interface& window() {
        return _window;
    }
    vector< mapable >& mapables() {
        return _mapables;
    }
    vector< column >& columns() {
        return _columns;
    }
    
private:
    
    std::string json_path;
    ofxJSONElement json;
    
    std::string _ctrl_osc_out_ip;
    uint16_t _ctrl_osc_out_port;
    uint16_t _ctrl_osc_in_port;
    
    std::string _max_osc_out_ip;
    uint16_t _max_osc_out_port;
    uint16_t _max_osc_in_port;
    
    float _column_touched_threshold;
    
    window_interface _window;
    
    vector< column > _columns;
    vector< mapable > _mapables;
    arduino_data_vector _arduino_data;
    

};

#endif /* FLATMAP_H */

