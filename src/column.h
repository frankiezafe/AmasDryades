/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   column.h
 * Author: frankiezafe
 *
 * Created on June 17, 2018, 12:26 PM
 */

#ifndef COLUMN_H
#define COLUMN_H

#include "common.h"
#include "mapable.h"
#include "tween.h"

#define COLUMN_SENSOR_COUNT 4

class column {
    
public:
    
    column();
    
    column( column_data& src );
    
    virtual ~column();
    
    mapable* pick_mapable();
    
    void tween_multiply( const uint64_t& duration, const float& value );
    
    /* Call this to synchronise the _threaded_sensors and 
     * _sensors. This will update values in _sensors and
     * configuration in _threaded_sensors.
     * Beware to do this during a global lock, or it will jam...
     */
    void sync();
    
    /* Update _threaded_sensors, call by arduinos
     * Call sync() to transfer data in _sensors in main thread.
     */
    float update_thread_sensor( uint8_t id, float v );
    
    void sensor_min( uint8_t id, float v );
    
    void sensor_max( uint8_t id, float v );
    
    std::string json();
    
    /* Clear all registered mapables, called by column manager
     * during mesh build.
     */
    void clear_mapables();
    
    /* Register a connected mapable, called by column manager
     * during mesh build.
     */
    void add_connector( mapable* m );
    
    /* Register a direct mapable, display sensor input
     */
    void add_direct( mapable* m );
    
    void deactivate();
    
    void activate();
    
    void shutdown();
    
    void sensor_multiply( const float& m );
    
    void UID( const uint32_t& p ) {
        _UID = p;
    }
    void arduino_id( const uint16_t& p ) {
        _arduino_id = p;
    }
    void analog_pin( const uint16_t& p ) {
        _analog_pin = p;
    }
    void touched_threshold( const float& tt ) {
        _touched_threshold = tt;
    }
    
    const uint32_t& UID() const {
        return _UID;
    }
    const bool& data_received() const {
        return _data_received;
    }
    const uint16_t& arduino_id() const {
        return _arduino_id;
    }
    const uint16_t& analog_pin() const {
        return _analog_pin;
    }
    const photo_resistor& operator[](uint8_t i ) const {
        return _sensors[i];
    }
    vector< mapable* >& mapables() {
        return _mapables;
    }
    
    bool touched();
    
    float highest_sensor();
    
    void entry_mapable( mapable* m );
    
private:
    
    uint32_t _UID;
    
    bool _active;
    bool _threaded_received;
    bool _data_received;
    uint16_t _arduino_id;
    uint16_t _analog_pin;
    
    float _touched_threshold;
    vector< photo_resistor* > _touched;
    
    /* List used by the arduino to push values */
    vector<photo_resistor> _threaded_sensors;
    /* List used by main thread to draw on screen */
    vector<photo_resistor> _sensors;
    /* Connected mapables */
    vector< mapable* > _mapables;
    /* Active mapable */
    vector< mapable* > _active_mapables;
    /* Mapable that activated this column*/
    vector< mapable* > _entry_mapable;
    /* Mapable only linked to this column */
    vector< mapable* > _direct;
    
    vector< tween > _tweens_multiply;
    
    void all_entry_up();
    
    void update_directs();

};

#endif /* COLUMN_H */

