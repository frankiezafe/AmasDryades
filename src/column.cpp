/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   column.cpp
 * Author: frankiezafe
 * 
 * Created on June 17, 2018, 12:26 PM
 */

#include <stdint.h>

#include "column.h"
#include "common.h"

column::column() :
_UID(numeric_limits<uint32_t>::max()),
_active(false),
_threaded_received(false),
_data_received(false),
_arduino_id(0),
_analog_pin(0),
_touched_threshold(PhotoResistor_touched) {

    _touched.resize(COLUMN_SENSOR_COUNT);
    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        _touched[i] = 0;
    }
    _sensors.resize(COLUMN_SENSOR_COUNT);
    _tweens_multiply.resize(COLUMN_SENSOR_COUNT);
    _threaded_sensors.resize(COLUMN_SENSOR_COUNT);

}

column::column(column_data& src) :
_UID(src.UID),
_active(false),
_threaded_received(false),
_arduino_id(src.arduino_id),
_analog_pin(src.analog_pin) {

    _touched.resize(COLUMN_SENSOR_COUNT);
    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        _touched[i] = 0;
    }
    _sensors.resize(COLUMN_SENSOR_COUNT);
    _tweens_multiply.resize(COLUMN_SENSOR_COUNT);
    _threaded_sensors.resize(COLUMN_SENSOR_COUNT);
    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        photo_resistor& pr = _sensors[i];
        if (i < src.sensors.size()) {
            pr.min = src.sensors[i].min;
            pr.max = src.sensors[i].max;
        }
        _threaded_sensors[i] = _sensors[i];
    }

}

column::~column() {
}

void column::tween_multiply(const uint64_t& duration, const float& value) {

    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        _tweens_multiply[i].init(&(_sensors[i].multiply));
        _tweens_multiply[i].start(duration, value);
    }

}

void column::sync() {

    _data_received = _threaded_received;

    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        _tweens_multiply[i].update();
        _threaded_sensors[i].config(_sensors[i]);
        _sensors[i].values(_threaded_sensors[i]);
    }

    // adapting directs
    update_directs();

}

void column::update_directs() {

    if (_direct.empty()) {
        return;
    }
    
    float hi = highest_sensor();
    vector< mapable* >::iterator it = _direct.begin();
    vector< mapable* >::iterator ite = _direct.end();
    for (; it != ite; ++it) {
        (*it)->force_min0(hi);
    }

}

float column::update_thread_sensor(uint8_t id, float v) {

    if (id >= COLUMN_SENSOR_COUNT) {
        return 0;
    }

    _threaded_received = true;

    photo_resistor& pr = _threaded_sensors[id];
    pr.value_raw = v;
    pr.value_norm = (pr.value_raw - pr.min) / (pr.max - pr.min);
    pr.value_norm = max(0.f, min(1.f, pr.value_norm)) * pr.multiply;

    return pr.value_norm;

}

void column::sensor_min(uint8_t id, float v) {

    if (id > 3) {
        return;
    }

    _sensors[id].min = v;

}

void column::sensor_max(uint8_t id, float v) {

    if (id > 3) {
        return;
    }

    _sensors[id].max = v;

}

std::string column::json() {

    std::stringstream ss;
    ss << "{" <<
            "\"UID\":" << _UID << "," <<
            "\"arduino_id\":" << _arduino_id << "," <<
            "\"analog_pin\":" << _analog_pin << "," <<
            "\"sensors\":[";
    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        if (i > 0) {
            ss << ",";
        }
        photo_resistor& pr = _sensors[i];
        ss << "{\"min\":" << pr.min << "," <<
                "\"max\":" << pr.max << "}";
    }
    ss << "]}";
    return ss.str();

}

void column::clear_mapables() {

    _mapables.clear();
    _active_mapables.clear();
    _entry_mapable.clear();
    _direct.clear();

}

void column::add_connector(mapable* m) {

    _mapables.push_back(m);

}

void column::add_direct(mapable* m) {

    _direct.push_back(m);
    m->load(MAPABLE_SHADER_CONFIG_DIRECT, false);

}

void column::deactivate() {

    _active = false;
    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        _touched[i] = 0;
    }
    _active_mapables.clear();
    _entry_mapable.clear();
    sensor_multiply(0);

}

void column::activate() {

    _active = true;
    sensor_multiply(1);

    vector< mapable* >::iterator it = _direct.begin();
    vector< mapable* >::iterator ite = _direct.end();
    for (; it != ite; ++it) {
        (*it)->load(MAPABLE_SHADER_CONFIG_DIRECT, false);
    }
    update_directs();

}

void column::sensor_multiply(const float& m) {

    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        _tweens_multiply[i].stop();
        _sensors[i].multiply = m;
    }

}

bool column::touched() {

    bool all_touched = true;
    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        if (!_touched[i]) {
            all_touched = false;
            break;
        }
    }
    if (all_touched) {
        return false;
    }

    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        if (
                !_touched[i] &&
                _sensors[i].value_norm > _touched_threshold
                ) {
            _touched[i] = &_sensors[i];
            std::cout << "sensor " << int(i) << " : " <<
                    _sensors[i].value_norm <<
                    " . " << _touched_threshold <<
                    std::endl;
            all_entry_up();
            return true;
        }
    }
    return false;

}

float column::highest_sensor() {

    float highest = 0;
    for (uint8_t i = 0; i < COLUMN_SENSOR_COUNT; ++i) {
        if (highest < _sensors[i].value_norm) {
            highest = _sensors[i].value_norm;
        }
    }
    return highest;

}

mapable* column::pick_mapable() {

    if (_active_mapables.size() == _mapables.size()) {
        std::cout << "column::pick_mapable : no more _mapables" << std::endl;
        return 0;
    }

    size_t i = size_t(ofRandom(0, _mapables.size()));
    while (i >= _mapables.size()) {
        i -= _mapables.size();
    }
    mapable* m = _mapables[ i ];
    while (
            std::find(_active_mapables.begin(), _active_mapables.end(), m) !=
            _active_mapables.end()
            ) {
        size_t i = size_t(ofRandom(0, _mapables.size()));
        while (i >= _mapables.size()) {
            i -= _mapables.size();
        }
        m = _mapables[ int( ofRandom(0, _mapables.size())) ];
    }

    _active_mapables.push_back(m);
    m->load(MAPABLE_SHADER_CONFIG_RISING, uint32_t(m->colB()) == UID());

    return m;

}

void column::entry_mapable(mapable* m) {

    if (std::find(_active_mapables.begin(), _active_mapables.end(), m) ==
            _active_mapables.end()
            ) {
        std::cout << "registering mapable " << m->colA() << " > " << m->colB() << std::endl;
        _active_mapables.push_back(m);
        _entry_mapable.push_back(m);
    }
}

void column::all_entry_up() {

    vector< mapable* >::iterator it = _entry_mapable.begin();
    vector< mapable* >::iterator ite = _entry_mapable.end();
    for (; it != ite; ++it) {

        mapable* m = (*it);
        m->load(MAPABLE_SHADER_CONFIG_SLOWON, uint32_t(m->colA()) == UID());
        std::cout << m << " slow_up" << std::endl;

    }

}