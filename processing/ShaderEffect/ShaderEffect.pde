void setup() {

  size( 200, 500 );
  
}

void draw() {

  // C++
  float timer_mult = 0.05;
  float timer = millis() * timer_mult;
  
  // shader
  float pow0  =    1.0;
  float pow1  =    0.0;
  float phase0 =   0.0;
  float phase1 =   0.0;
  float min0 =     0.0;
  float min1 =     0.0;
  
  float freq_mult = -20000;
  
  loadPixels();
  
  int i = 0;
  
  for ( int y = 0; y < height; ++y ) {
    
    float h = y * 1.f / height;
    
    for ( int x = 0; x < width; ++x ) {
    
      float w = x * 1.f / width;
      
      float pow_top = ( 1 - h ) * ( 1 + sin( ( timer + h * pow( ( 1 + cos( PI - phase0 * PI ) ), 2 ) * freq_mult ) * timer_mult) ) * 0.5 * pow0;
      float pow_bottom = h * ( 1 + sin( ( timer + h * pow( ( 1 + cos( PI - phase1 * PI ) ), 2 ) * freq_mult ) * timer_mult) ) * 0.5 * pow1;
      
      float pow = ( 1 - h ) * ( min0 + ( 1 - min0 ) * pow_top ) +  h * ( min1 + ( 1 - min1 ) * pow_bottom );
      //float pow = pow_top + pow_bottom;
      
      w = 1 - ( ( 1 + cos( w * TWO_PI ) ) * 0.4 );
      float r = pow * 255;
      
      //pixels[ i ] = color( r * w, min0 * 255, r * w );
      pixels[ i ] = color( r * w );
      
      ++i;
      
    }
    
  }
  
  updatePixels();

}