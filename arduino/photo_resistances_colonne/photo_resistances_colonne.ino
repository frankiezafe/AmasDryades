int digital_controllers_count;
int* digital_controllers;

int analog_readers_count;
int* analog_readers;

int digital_current_index;

unsigned long now;
unsigned long last_time;
unsigned long delta_time;
unsigned long wait;
unsigned long wait_time;

int id = 0;
char id_buffer[7];

void setup() {

    Serial.begin( 9600 );
    digital_controllers_count = 4;
    digital_controllers = new int[ digital_controllers_count ];
    digital_controllers[0] = 10;
    digital_controllers[1] = 11;
    digital_controllers[2] = 12;
    digital_controllers[3] = 13;

    // tous à zéro

    for ( int i = 0; i < 4; ++i ) {
      pinMode( digital_controllers[i] , OUTPUT );
      digitalWrite( digital_controllers[i] , LOW );
    }

    analog_readers_count = 6;
    analog_readers = new int[ analog_readers_count ];
    analog_readers[0] = A0;
    analog_readers[1] = -1; // A1;
    analog_readers[2] = -1; // A2;
    analog_readers[3] = -1; // A3;
    analog_readers[4] = -1; // A4;
    analog_readers[5] = -1; // A5;

    digital_current_index = 0;
    now = 0;
    last_time = 0;
    delta_time = 0;

    wait = 0;
    wait_time = 250;

    itoa( id, id_buffer, 10 );

}

void loop() {

    now = millis();
    if ( last_time == 0 ) {
      last_time = now;
      return;
    } else {
      delta_time = now - last_time;
      last_time = now;
    }

    // Serial.println( wait );
    wait += delta_time;
    
    if ( wait >= wait_time  ) {
      
      wait = 0;
      
      for ( int i = 0; i < analog_readers_count; ++i ) {
        
        if ( analog_readers[i] == -1 ) {
          continue;
        }

        char output[7*3];

        char sensor_buffer[7];
        itoa( i, sensor_buffer, 10 ); //(integer, yourBuffer, base)
        
        int value = analogRead( analog_readers[i] );
        char value_buffer[7]; //the ASCII of the integer will be stored in this char array
        itoa( value, value_buffer, 10 ); //(integer, yourBuffer, base)

        for( int i = 0; i < 7; ++i ) {
            output[i] = id_buffer[i];
            output[i+7] = sensor_buffer[i];
            output[i+14] = value_buffer[i];
        }
        
        Serial.println( output );
        
//        string s;
//        s += itao( id );
//        s += ' ';
//        s += atoi( i );
        Serial.print( id );
        Serial.print( " " );
        Serial.print( i );
        Serial.print( " " );
        Serial.print( digital_current_index );
        Serial.print( " " );
        Serial.println( value );

      }
  
      digitalWrite( digital_controllers[digital_current_index] , LOW );
      digital_current_index = ( digital_current_index + 1 ) % digital_controllers_count;
      digitalWrite( digital_controllers[digital_current_index] , HIGH );
      // Serial.println( digital_current_index );

    }

}

//digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
//delay(1000);              // wait for a second
//digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
//delay(1000);              // wait for a second
