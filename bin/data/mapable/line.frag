#version 120

const float PI = 1.57079635;

uniform float timer;
uniform float pow0;
uniform float pow1;
uniform float phase0;
uniform float phase1;
uniform float min0;
uniform float min1;
uniform float freq;
uniform float tex0weight;
uniform vec2 tex0size;
uniform sampler2DRect tex0;
uniform vec4 rgba;

void main() {

    vec2 pos = gl_TexCoord[0].xy;
    vec4 color = texture2DRect(tex0, pos * tex0size);
    
    float amult0 = ( 1.0 + sin( timer + pos.y * cos( PI - phase0 * PI ) * freq ) ) * 0.5 * pow0;
    float amult1 = ( 1.0 + sin( timer + pos.y * cos( PI - phase1 * PI ) * freq ) ) * 0.5 * pow1;
    
    float amult = (1-pos.y) * ( min0 + ( 1.0 - min0 ) * amult0 ) + pos.y * ( min1 + ( 1.0 - min1 ) * amult1 );

    gl_FragColor = 
        color * vec4(1,1,1,amult) * rgba * tex0weight + 
        vec4(1,1,1,amult) * rgba * (1-tex0weight);
    
}